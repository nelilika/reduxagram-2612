import './Dashboard.scss';
import NavBar from '../../components/UI/NavBar/NavBar';
import { Outlet } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

function Dashboard() {
  const navigate = useNavigate();

  function navigateToLogin() {
    navigate('/login');
  }
  return (
    <div className="dashboard-wrapper">
      <div className="login-header">
        <div className="name-wrapper">
          <h3>Hello, Liliia !</h3>
        </div>
        <button onClick={navigateToLogin} className="custom-btn btn-1">
          <span>Log in</span>
        </button>
      </div>

      <NavBar />
      <Outlet />
    </div>
  );
}

export default Dashboard;
