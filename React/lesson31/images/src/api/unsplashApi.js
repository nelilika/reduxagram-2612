// Запити на сервер
import axios from 'axios';
import { ACCESS_KEY } from '../config/config';

// using fetch
// export function getImagesByName(query, per_page = 20) {
//   return fetch(`https://api.unsplash.com/search/photos/?query=${query}&per_page=${per_page}`, {
//     headers: {
//       Authorization: `Client-ID ${ACCESS_KEY}`,
//     },
//   }).then((res) => res.json());
// }

// using axios
const unsplashAPI = axios.create({
  baseURL: 'https://api.unsplash.com/',
  headers: {
    Authorization: `Client-ID ${ACCESS_KEY}`,
  },
});

export function getImagesByName(query, per_page = 20) {
  return unsplashAPI.get('/search/photos', {
    params: {
      query,
      per_page,
    },
  });
}
