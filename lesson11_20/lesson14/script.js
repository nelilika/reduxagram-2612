console.log(document); // html document
console.log(document.body); // body tag

console.dir(document);
console.dir(document.body.style);

const obj = {
  start: 'wr',
  2: 3345,
  'start-start': 'ete',
};

obj.start;
obj[2];
obj['start-start'];

document.body.style.backgroundColor = 'aquamarine';
// document.body.style.textAlign = 'center';

const divElements = document.querySelectorAll('div');
console.log(divElements);

const [, pElement] = document.querySelectorAll('div.card-info p');
pElement.style.color = 'blue';

// Зміна тексту
console.log('innerHTML', pElement.innerHTML);
console.log('innerText', pElement.innerText);
// console.log('outerHTML', pElement.outerHTML);
console.log('textContent', pElement.textContent); // recomenended

pElement.textContent += 'New Text';
pElement.setAttribute('new-attribute', 'hello');
console.log(pElement.getAttribute('data-info'));

const header = document.querySelector('.card-header');

const button = document.createElement('button');
button.textContent = 'New button before';
button.style.backgroundColor = '#7777ab';

// header.before('New Text before h1'); // додасть текст
header.before(button);

const button2 = document.createElement('button');
button2.textContent = 'New button after';
button2.style.backgroundColor = '#ab7777';

header.after(button2);
// header.after(button); // то він видалиться before h1 і додасться after h1

// header.appendChild() === header.append()
// aле

const headers1 = document.querySelectorAll('h1');

for (let header of headers1) {
  // header.className += ' header';
  header.classList.add('header');
  // header.classList.add('header'); - один і той самий клас можна додати тільки один раз
  // header.classList.remove()
  console.log(header.classList.contains('card-header'));

  // header.classList.toggle('header');
  console.dir(header);
}
