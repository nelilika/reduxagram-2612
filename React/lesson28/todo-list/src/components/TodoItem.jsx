import React, { useState } from 'react';
import { FiEdit2, FiX } from 'react-icons/fi';
import AddTodo from './AddTodo';

export default function TodoList({
  todo,
  deleteTodo,
  toggleCompletedTodo,
  updateTodo,
}) {
  const [editedTodo, setEditedTodo] = useState(null);
  const classes = ['todo-row'];

  if (todo.isCompleted) {
    classes.push('complete');
  }

  if (editedTodo) {
    return (
      <AddTodo
        editedTodo={editedTodo}
        setEditedTodo={setEditedTodo}
        updateTodo={updateTodo}
      />
    );
  }

  function handleDelete() {
    /*eslint no-restricted-globals: ["error", "event", "fdescribe"]*/
    const isConfirmed = confirm(`Are you ready to remove "${todo.text}"`);

    if (!isConfirmed) return;
    deleteTodo(todo.id);

    // if (isConfirmed) {
    //   deleteTodo(todo.id);
    // }
  }

  function toggleComplete() {
    toggleCompletedTodo(todo.id);
  }

  return (
    <div key={todo.id} className={classes.join(' ')}>
      <div onClick={toggleComplete}>{todo.text}</div>
      <div className="icons">
        <FiX onClick={handleDelete} />
        {!todo.isCompleted && (
          <FiEdit2
            onClick={() => setEditedTodo({ ...todo })}
            style={{ width: '20px' }}
          />
        )}
      </div>
    </div>
  );
}
