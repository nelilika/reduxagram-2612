// Неявні перетворення типів даних
// Обʼекти і масиви

// Обʼєкт
// Object -> Boolean
// завжди true

Boolean({}); // true
Boolean({ a: 1 }); // true

// Object -> Number
// obj.valueOf(); - зробити перетворення в Number
// але не може цього зробити і обʼєект залишається без змін

// Object -> String
String({}); // [object Object] -> для будь-якого обʼєкту

// Массив
// Array -> Boolean
// завжди true

Boolean([]); // true
Boolean([1, 2, 3]); // true

// Array -> Number
// arr.valueOf(); - зробити перетворення в Number
// але не може цього зробити і масив залишається без змін

// Array -> String
String([]); // ''
String([1, 2, 3]); // '1,2,3'
String([1, 2, {}]); // '1,2,[object Object]'

Number({}); // NaN - Number('[object Object]')
'$' +
  { hello: 'world' } + // '$[object Object]
  { key: 'value' }; // NaN

// [] + {} -> '[object Object]'
// {} + [] -> 0

const obj = {};
console.log({} + 5);
{
}
+5(
  // 5
  {} + 5
); // '[object Object]5'
5 === {} + 5; // false
