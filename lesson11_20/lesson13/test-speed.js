const number = 100;
const array = [...new Array(1000)].map(() => getRandomInt(1, number));
// Другий спосіб створити і заповнити масив
// const array = Array.from({ length: number }, () => getRandomInt(1, number));

// Test case 1
console.group('Sum of elements:');
console.time('For');
let sumFor = 0;
for (let i = 0; i < array.length; i++) {
  sumFor += array[i];
}
console.timeEnd('For');

console.time('Reduce');
const sumReduce = array.reduce((acc, item) => acc + item);
console.timeEnd('Reduce');

console.log('Equal results: ', sumReduce === sumFor);
console.groupEnd('Summ of elements:');

// Test case 2
console.group('Join elements:');
console.time('For');
let strFor = '';
for (let i = 0; i < array.length; i++) {
  strFor += array[i];
}
console.timeEnd('For');

console.time('Method Join');
const joinArray = array.join('');
console.timeEnd('Method Join');

console.time('Reduce');
const joinReduce = array.reduce((acc, item) => acc + item, '');
console.timeEnd('Reduce');
console.log(
  'Equal results: ',
  strFor === joinArray && joinArray === joinReduce
);
console.groupEnd('Join elements:');

// Test case 3
console.group('Filter and change elements:');
console.time('For');
let filterChangeFor = [];
for (let i = 0; i < array.length; i++) {
  if (i > 100) {
    filterChangeFor[i] = String(array[i]);
    // filterChangeFor.push(String(i));
  }
}
console.timeEnd('For');

console.time('Method filer + map');
const filterMapArray = array
  .filter((item) => item > 100)
  .map((item) => String(item));
console.timeEnd('Method filer + map');

console.time('Reduce + spread');
const filterMapSpread = array.reduce((acc, item) => {
  if (item > 100) {
    return [...acc, String(item)];
  }
  return acc;
}, []);
console.timeEnd('Reduce + spread');

console.time('Reduce + push');
const filterMapPush = array.reduce((acc, item) => {
  if (item > 100) {
    acc.push(String(item));
  }
  return acc;
}, []);
console.timeEnd('Reduce + push');
console.groupEnd('Filter and change elements:');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}
