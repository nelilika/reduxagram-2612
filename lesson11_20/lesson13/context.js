'use strict';

// Загублення контексту

const weather = {
  min: -50,
  max: 48,
  init: 'celsius',
  getRange() {
    console.log(this);
    return this.max - this.min; // 98
  },
  logDegress() {
    console.log(`Max: ${this.max}; Min: ${this.min}`);
  },
  setDegress(newMin) {
    this.min = newMin;
    this.max = Math.random() * 10;
  },
  parseDegress() {
    return `${this.min} degrees`;
  },
  trickLogDegress() {
    const log = function () {
      console.log('inside', this);
      console.log(`Max: ${this.max}; Min: ${this.min} in one second`);
    };
    console.log('outside', this);

    setTimeout(log.bind(this), 1000);
  },
  trickArrowLogDegress() {
    console.log(this);
    setTimeout(() => {
      console.log('inside', this);
      console.log(`Max: ${this.max}; Min: ${this.min} in one second`);
    }, 1000);
  },
};

function getRange() {
  console.log(this); // window
  return this.max - this.min; // NaN
}
// console.log(getRange());

// log();
