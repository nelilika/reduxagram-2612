const logger = (store) => (next) => (action) => {
  console.warn('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  return result;
};

const middlewares = [];

console.log(process.env.NODE_ENV);

if (process.env.NODE_ENV === 'development') {
  middlewares.push(logger);
}

export { middlewares };
