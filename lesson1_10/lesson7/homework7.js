const employees = [
  {
    id: 1,
    name: 'Karan',
    surname: 'Duffy',
    salary: 1010,
    workExperience: 10,
    isPrivileges: false,
    gender: 'male',
  },
  {
    id: 2,
    name: 'Brax',
    surname: 'Dalton',
    salary: 1200,
    workExperience: 12,
    isPrivileges: false,
    gender: 'male',
  },
  {
    id: 3,
    name: 'Jody',
    surname: 'Lam',
    salary: 480,
    workExperience: 3,
    isPrivileges: true,
    gender: 'female',
  },
  {
    id: 4,
    name: 'Ceara',
    surname: 'Zuniga',
    salary: 2430,
    workExperience: 20,
    isPrivileges: false,
    gender: 'female',
  },
  {
    id: 5,
    name: 'Walid',
    surname: 'Hulme',
    salary: 3150,
    workExperience: 30,
    isPrivileges: true,
    gender: 'female',
  },
  {
    id: 6,
    name: 'Hilary',
    surname: 'Oliver',
    salary: 1730,
    workExperience: 15,
    isPrivileges: false,
    gender: 'male',
  },
  {
    id: 7,
    name: 'Tye',
    surname: 'Herring',
    salary: 5730,
    workExperience: 45,
    isPrivileges: true,
    gender: 'male',
  },
  {
    id: 8,
    name: 'Marc',
    surname: 'Ellison',
    salary: 4190,
    workExperience: 39,
    isPrivileges: false,
    gender: 'male',
  },
  {
    id: 9,
    name: 'Deon',
    surname: 'Lindsay',
    salary: 790,
    workExperience: 7,
    isPrivileges: false,
    gender: 'male',
  },
  {
    id: 10,
    name: 'Marlene',
    surname: 'Rogers',
    salary: 5260,
    workExperience: 49,
    isPrivileges: true,
    gender: 'female',
  },
  {
    id: 11,
    name: 'Rodrigo',
    surname: 'Kouma',
    salary: 300,
    workExperience: 1,
    isPrivileges: false,
    gender: 'male',
  },
];

/* Завдання 1.
Напишіть програму, яка продемонструє роботу з масивом. Створіть масив із восьми елементів:
  '455' 87.15 true undefined null 'false' [] {}
Виведіть інформацію о типі даних кожного елемента в консолі.
Додайте значення 7 до кожного елементу масива і виведіть отримані значення в консолі.
*/
{
  const testArray = ['455', 87.15, true, undefined, null, 'false', [], {}];
  const numberToAdd = 7;

  for (let value of testArray) {
    if (value === null) {
      console.log('Element: ', value, 'null');
      console.log('Sum: ', value + numberToAdd);
    } else {
      const valueType = typeof value;
      console.log('Element: ', value, valueType);
      console.log('Sum: ', value + numberToAdd);
    }
  }
}

/* Завдання 2
  Напишіть програму, яка питає у користувача число і створює масив numbers з випадкових цілих чисел в діапазоні від 0 до 10,
  довжина якого дорівнює числу, яке ввів користувач.
  Виведіть створений масив numbers в консолі.
  Скопіюйте массив numbers в новий масив. Кожен третій елемент нового масиву помножте на 3.
  Виведіть новостворений масив в консолі.
*/

{
  const userNumberForLength = +prompt(
    'Please enter an integer positive number for array length:',
    '5'
  );
  const userLengthArray = [];

  if (
    isNaN(userNumberForLength) ||
    userNumberForLength <= 0 ||
    !Number.isInteger(userNumberForLength)
  ) {
    console.error('Please use positive integer numbers greater than 0!');
  } else {
    for (let i = 0; i < userNumberForLength; i++) {
      const randomNumber = Math.round(Math.random() * 10);
      userLengthArray[i] = randomNumber;
    }
    console.log('Your array is', userLengthArray);

    const newArray = [...userLengthArray];
    for (let i = 2; i < newArray.length; i += 3) {
      newArray[i] *= 3;
    }
    console.log('Your new array is', newArray);
  }
}

/* Завдання 3
  а) Створіть массив, який складається з повних імен всіх співробітників.
*/

{
  const fullNames = [];
  for (let i = 0; i < employees.length; i++) {
    fullNames[i] = `${employees[i].name} ${employees[i].surname}`;
  }
  console.log('The list of employees full names is:', fullNames);
}

/*
b) Знайдіть середнє значення всіх зарплат співробітників.
*/

{
  let salarySum = 0;

  for (let value of employees) {
    salarySum += value.salary;
  }

  const average = salarySum / employees.length;
  console.log(`The average salary is ${average.toFixed(2)}`);
}

/*
с) Виведіть в консоль імʼя чоловіка-пільговика (ключ isPrivileges=true)
з самою великою зарплатою.
*/

{
  let maxPrivilegesMan;
  let maxPrivManSalary = 0;

  for (let value of employees) {
    if (
      value.isPrivileges &&
      value.gender === 'male' &&
      value.salary > maxPrivManSalary
    ) {
      maxPrivManSalary = value.salary;
      maxPrivilegesMan = `${value.name} ${value.surname}`;
    }
  }
  console.log(
    `The man with the privilege & highest salary is ${maxPrivilegesMan}`
  );
}

/*
d) Виведіть в консоль повні імена (імʼя + прізвище) двох жінок з самим маленьким
досвідом роботи (ключ workExperience).
*/

{
  const women = [];
  for (let employee of employees) {
    if (employee.gender === 'female') {
      women.push(employee);
    }
  }
  let firstWoman = women[0];
  let secondWoman = women[1];
  let thirdWoman = 0;

  for (let i = 2; i < women.length; i++) {
    thirdWoman = women[i];

    if (secondWoman.workExperience < firstWoman.workExperience) {
      firstWoman = secondWoman;
      secondWoman = thirdWoman;
    } else {
      secondWoman = thirdWoman;
    }
  }

  console.log(
    `Name of the first female employee with the least work experience: ${firstWoman.name} ${firstWoman.surname}`
  );
  console.log(
    `Name of the second female employee with the least work experience: ${secondWoman.name} ${secondWoman.surname}`
  );
}

/*
e) Виведіть в консоль інформацію, скільки всього заробили співробітники за весь час роботи в одній строці.
Формат відповіді: <імʼя прізвище> - <сума>.
*/

{
  for (let value of employees) {
    const result = value.workExperience * value.salary;
    console.log(`${value.name} ${value.surname} - ${result}`);
  }
}
