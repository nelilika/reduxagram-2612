/* Задача 1.
Написати програму, яка тричі питає у користувача будь-яку інформацію і виводить її в консоль в одному рядку
Наприклад, для рядків "JavaScript', 'is my favorite', 'language', в консолі отримаємо
*/

{
  const string1 = prompt('Enter the first string:', 'JavaScript');
  const string2 = prompt('Enter the second string:', 'is my favorite');
  const string3 = prompt('Enter the third string:', 'language');

  console.log(phraseOne + phraseTwo + phraseThree);
}

/* Задача 2.
Напишіть програму, яка запитує у користувача кількість годин і переводить 
години в секунди. Отримані секунди вивести в модальне окно командою alert();
 */

{
  const hours = parseFloat(
    prompt(
      'Enter any quantity of hours which you would like to convert to seconds:'
    )
  );
  alert(`Your amount is ${hours * 60 * 60} seconds`);
}

/* Задача 3
Напишіть програму, яка питає у користувача номер квартири (команда prompt())
і виводить в консоль номер поверху і номер під'їзду.
Відомо, що в одному під'їзді 9 поверхів по 3 квартири на кожному поверсі.
Результат (поверх і під'їзд) відобразіть в консолі (команда console.log()).

  const roomsOnFloor = 3;
  const floors = 9;
  const roomNumber = prompt();
*/

{
  const roomsOnFloor = 3;
  const floors = 9;
  const roomNumber = +prompt('Enter the room number', 456);

  const userFloor = Math.ceil((roomNumber / roomsOnFloor) % floors);
  const userEntrance = Math.ceil(roomNumber / (roomsOnFloor * floors));

  console.log(`
    Your floor: ${userFloor}.
    Your entrance: ${userEntrance}.
  `);
}

/* Задача 4.
Напишіть програму, яка питає у користувача температуру в Цельсіях 
и конвертує її в Фаренгейти. Результат відобразіть в консолі.
Формула для конвертації: Цельсій х 1,8 + 32 = Фаренгейт
tips: градус Цельсія в юнікоді буде "\u2103", щоб відобразити його в строці. 
Спробуйте самостійно знайти символ для позначення градусу Фаренгейта.
  60°C is 140°F
*/

{
  const celsius = parseFloat(prompt('Enter degrees in celsius'));
  const fahrenheit = (celsius * 1.8 + 32).toFixed(2);

  console.log(`${celsius}\u2103 is ${fahrenheit}\u2109`);
}
