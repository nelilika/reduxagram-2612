// Типи даних JavaScript 8
// string
// number
// boolean
// undefined
// null

// object

// symbol
// bigInt

// Перетворення типів даних

// Математичні операції +, -, *, /, %, **
// Логічні операції >, <, >=, <=
// Операції з порівнянням ===, ==, !==

// const number = +prompt('Enter number'); // parseInt, parseFloat()
Number('2'); // 2 теж саме що і +
String(2) // '2'
Boolean(2) // true


// Number

// Number -> Boolean

// +0, -0, NaN -> false
// всі інші числа 2, 45, -56, Infinity -> true
console.log(Boolean(-45)); // true
console.log(Boolean(NaN)); // false

console.log('Number -> null, undefined');
console.log(Number(null)); // 0
console.log(Number(undefined)); // NaN

// Number -> String

// 45 -> '45'
// NaN -> 'NaN'
// Infinity -> 'Infinity'

console.log(String(NaN)); // 'NaN'

// String

// String -> Boolean
// '' -> false
// ' ' ,будь-що всередині рядка -> true
console.log(Boolean(' ')); // false
console.log(Boolean('false')); // true

// String -> Number
// '45' рядок, який містить ТІЛЬКИ числа -> в це число з типом number 45
// '-345' -> -346

// '45px', '-e45' -> NaN
console.log(Number('0001')); // 1
console.log(Number('1e3')); // 1000
console.log(Number('Infinity')); // Infinity

// Boolean

// true -> все інше
// false -> 0, NaN, '', null, undefined
console.log(Boolean(null)); // false
console.log(Boolean(undefined)); // false


// Математичні//Логічні операції

// 4 + 6 -> 10
// true - false -> 1 - 0 -> 1
// 'cat' - 1 -> NaN
// '567' / 0 * null + undefined -> 567 / 0 * 0 + NaN

// Всі математичні операції виконуються ВИКЛЮЧНО в типі Number
// Виключенн оператор +;

// +
// працює як сума виключно з числами
// працює як конкатенація з рядками

// Конкатинація - це злиття рядків в один великий рядок

console.log('cat' - 1); // NaN - робота з числами
console.log('cat' + 1); // cat1 - робота з рядками - конкатинація

console.log(17 - 14 + 'qwerty'); // 3 + 'qwerty' -> '3qwerty'
console.log(17 + 'qwerty' - 14); // '17qwerty' - 14 -> NaN

console.log(true > 34);

// порівняння з null
 
// Оператори порівняння
// == (порівняння)
// === (суворе порівняння) порівнює без неявного перетворення

// 17 == 45 -> false
// 'qwerty' == 'qwerty' -> true

'23' == 23 // true
'23' === 23 // false
'0001' == 1 // true
'0001' === 1 // false

// undefined/null
// порівняння з null/undefined завжди FALSE;
// Виключення:

// 1) null === null -> true
// 2) undefined === undefined -> true
// 3) undefined == null -> true !!!


// NaN завжди false в порівняннях
console.log(NaN == NaN);
console.log(NaN === NaN);

// Порівняння рядків
// якщо у виразі обидва операнди - рядки, то користуємося наступним правилом
// 'a', 'U', '1', '&' 
// кожен символ має свою вагу
// 'a' > 'U' > '1' > '&'
// де 'a' - найтяжча
// а спец символи '&' - найлегші

// однакові літери в регістру порівнюються за алфавітом
// де a - найлегша
// z - найважча

console.log('a' < 'A'); // false
console.log('A' > '1') // true
console.log('&' < '1') // true

console.log('cat' < 'dog'); // true

// Логічне НЕ
// !true -> false
// !5 -> false
// !NaN -> true

// !!'qwerty' - це теж саме що і Boolean('qwerty')

// null == 0 - false - бо null нічому не дорівнює, окрім nul;
// null > 0 - false - приводимо до числа і порівнюємо 0 > 0
// null >= 0 - true - за правилом виключення в EcmaScript


!!'summer' && prompt('Enter your value', 'rt') && typeof null;
// true && prompt('Enter your value', 'rt') && typeof null;
// true && true && true;
// typeof null; -> 'object'