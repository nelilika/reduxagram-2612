import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from './reducers/rootReducer';
import { middlewares } from './middlewares';

export const store = configureStore({
  reducer: rootReducer,
  middleware: [...middlewares],
});
