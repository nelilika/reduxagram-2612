const button = document.querySelector('.test-btn');

document.body.addEventListener(
  'click',
  function (event) {
    console.error('Body father');
    console.log(event.target);
  },
  {
    capture: true,
    once: true,
  }
);

// EventLoop
button.addEventListener('click', function (event) {
  console.log(event);
  console.warn('Hello from the click button');
});

console.log('Hello from the global scope');

const strongEl = document.querySelector('strong');
const p = document.querySelector('#textId');

strongEl.addEventListener('click', function (event) {
  // event.stopPropagation(); // припиняє всплиття подій
  console.log(event.target);
});

p.addEventListener('click', function (event) {
  console.warn('click on P');
  console.log(event.target);
});
