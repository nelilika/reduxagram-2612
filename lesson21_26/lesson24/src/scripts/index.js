// import * as first from './first';
import { arr, getName } from './first';
import Student from './default';
import '../styles/style.scss';

console.log(arr);
console.log('Hello world');

console.log(getName('Lily'));
getName('Arina Grande');

const student = new Student('Majk Vasovski', 23);
console.log(student.getInfo());
