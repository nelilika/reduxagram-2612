// Умовний опертор

// Треба перевірити, що користувач ввів ціле додатнє число
// type - number +
// number > 0 +
// Number('45') % 1 === 0 || parseInt('45') === Number('45') || Number('45').toFixed(0) === '45'


const number = prompt('Enter integer'); // string 

// if (typeof +number === 'number') {
//     console.log('typeof number');
// } // буде завджи виконуватись
// +'45' -> 45
// +'qwerty' -> NaN

// Variant 1 - raw
// if (isFinite(number)) {
//     console.log('hello ist number');

//     if (number > 0) {
//         console.log('its number > 0');

//         if (Number(number) % 1 === 0) {
//             console.log('its integer');
//         } else {
//             console.log('its float');
//         }
//     } else {
//         console.log(' its number < 0');
//     }
// } else {
//     console.log('Validation Error');
// }

// Variant 2
if (isFinite(number) && number > 0 && !(Number(number) % 1)) {
    console.log('its integer');
} else {
    console.log('Validation Error');
}

// Variant 3 - з використанням тернарного оператору

const result = isFinite(number) && number > 0 && !(Number(number) % 1) ? 'its integer' : 'Validation Error';
console.log(result);
// тернарний оператор ми використовуємо коли?
// коли в умові не більше двох (2) виразів
// коли код після умови займає не більше одного (1) рядка

let mark = 0; // початкове значення
while(mark <= 5) { // умова виконання і умова зупинки !
  console.log(`Mark: ${mark}`); // тіло циклу , або ітерація
let number = 0;  // тіло циклу , або ітерація
number++; // тіло циклу , або ітерація
console.log(number); // тіло циклу , або ітерація



  mark = mark + 2; // шаг ітерації
}


const date = new Date();
console.log(date);

const newDate = new Date('2020-05-07'); // створить дату 5 травня 2020
const day = 6;
const month = 1;
const year = 2021;

new Date(`${year}-${month}-${day}`); // cтворить дату 6 січня 2021 року

// switch / case
// В залежності від країни вивести на екран материк, де вона знаходиться

const country1 = 'Ukraine'; // Evrasia
const country2 = 'Brasil'; // South Amerika
const country3 = 'Canada'; // North Amerika
const country4 = 'Australia'; // Australia

switch (country3) {
    case 'Ukraine': { // if ( country1 === 'Ukraine') {}  'Yellow' != case 'yellow' використовується тільки сурове порівняння
        console.log('Evrasia');
        break;
    }
    case 'Brasil': {
        console.log('South Amerika');
        break;
    }
    case 'Canada': {
        console.log('North Amerika');
        break;
    }
    case 'Australia': {
        console.log('Australia');
        break;
    }
    default: {
        console.log('other');
    }
}


// Code style for dates
// 4 дня в секундах
const seconds = 345600;
const seconds2 = 4 * 24 * 60 * 60;

