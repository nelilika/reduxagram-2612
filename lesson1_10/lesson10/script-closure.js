// console.log(testValue); // можна тільки прочитати
someLogFunc(1, 4);
// Область видимості

// Глобальні змінні - змінна, яка доступна у всьому файлі
// Локальні змінні - змінна, яка доступна тільки в рамках блоку коду

let sum = 0; // глобальна змінна
for (var i = 0; i < 10; i++) {
  // локальна зміна
  sum += i;
  // console.log(sum);
}
// console.log(i); буде помилка - змінна вже не доступна
console.log(sum); // змінна доступна будь-де

// Де можуть створюватись локальні змінні
// цикли
// умовні конструкції
// функції

// Як можна створити змінну
// 1) let
// 2) const
// 3) var - застарілий метод (змінна вспливає) hoisting
// 4) без кльчового слова

console.log(testValue);
var testValue = 'Hello test value';
console.log(testValue);

function someLogFunc(a, b) {
  console.log(a + b);
}

// Змінні створені за допомогою var і функції вспливають наверх нашого файлу
// і доступні вже з першого рядка виконання нашого код

function localFunction() {
  var localVar = '5'; // змінна локальна
  console.log(localVar);
}
// console.log(localVar); // буде помилка - область видимості - ФУНКЦІЯ

(function () {
  var testIIFE = 'Hello from IIFE';
  console.log(testIIFE);
})();
// console.log(testIIFE); // буде помилка - область видимості - ФУНКЦІЯ
