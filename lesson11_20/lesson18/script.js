// Функція-конструктор
function CityFunction(name, population, isCapital) {
  // this = {}
  this.name = name;
  this.population = population;
  this.isCapital = isCapital;
  // return this;
  // НЕ ВИКОРИСТОВУВАТИ return у функціях-контруктор
  // return 2; // return this; - ignore 2
  /**
   * return { a: 1 } // return { a: 1 }
   */

  let x = 10;

  this.logInfo = function () {
    return `Hello from ${this.name}. There are ${this.population} people living here!`;
  };
}
{
  const city1 = new CityFunction('Kharkiv', 1.5e6, false);
  const city2 = new CityFunction('Kiyv', 3e6, true);
  const city3 = new CityFunction('Chornomorsk', 8e4, false);
  console.log(city1);
  console.log(city2);
  console.log(city3);
}
/**
 * {
 *   name: 'Kharkiv',
 *   population: 1500000,
 *   isCapital: false,
 *   logInfo: function() {} АБО
 *   logInfo() {}
 * }
 */

// Клас - recomended to use

class City {
  #counter = 45; // приватна властивість
  constructor(name, population, isCapital) {
    // this
    this.name = name;
    this.population = population;
    this.isCapital = isCapital;
  }

  get counter() {
    // можливіть отримати значення приватного методу - геттер
    return this.#counter;
  }

  set counter(value) {
    // можливіть змінити значення приватного методу - сеттер
    this.#counter = value;
  }

  publicLogInfo() {
    // публічний метод
    this.#counter = 50;
    return this.#logInfo();
  }

  #logInfo() {
    // приватний метод
    return `Hello from ${this.name}. There are ${this.population} people living here!`;
  }
}

class District extends City {
  constructor(name, population) {
    // this створюватись не буде
    super(); // наслідує від батьківського класу this
    this.name = name; // super === this
    this.population = population;
  }
  // наслідується і доступний геттер і сеттер City
  // наслідується метод publicLogInfo тільки
}

const city1 = new City('Kharkiv', 1.5e6, false);
const city2 = new City('Kiyv', 3e6, true);
const city3 = new City('Chornomorsk', 8e4, false);
console.log(city1);
console.log(city2);
console.log(city3);
const district = new District('Moskaliovka', 7e3);
console.log(district);

// приклад наслідування. Цей метод існує, бо написаний у батьківського класа
district.publicLogInfo();
