import axios from 'axios';
import { INSTAGRAM_URL } from '../config';

const instagramAxios = axios.create({
  baseURL: INSTAGRAM_URL,
  timeout: 1000,
});

export const getPosts = async function ({ limit: _limit, page: _page }) {
  const { data, headers } = await instagramAxios.get('/posts', {
    params: {
      _limit,
      _page,
    },
  });
  const totalCount = Number(headers['x-total-count']);

  return { data, totalCount };
};

export const getPost = function (id) {
  return instagramAxios.get(`/posts/${id}`);
};

export const getComments = async function (id) {
  const { data: comments } = await instagramAxios.get(`/posts/${id}/comments`);
  return { comments };
};
