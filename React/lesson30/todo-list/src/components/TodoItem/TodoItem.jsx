import React, { useState, useContext } from 'react';
import { FiEdit2, FiX } from 'react-icons/fi';
import AddTodo from '../AddTodo/AddTodo';
import { TodoContextComponent } from '../../context/Context';
import { deleteTodo, completeTodo } from '../../reducer/todoReducer';
import PropTypes from 'prop-types';
import todoItemClasess from './TodoItem.module.scss';

export default function TodoItem({ todo }) {
  const [editedTodo, setEditedTodo] = useState(null);
  const [, dispatch] = useContext(TodoContextComponent);
  const classes = [todoItemClasess.todoRow];

  if (todo.isCompleted) {
    classes.push(todoItemClasess.complete);
  }

  if (editedTodo) {
    return <AddTodo editedTodo={editedTodo} setEditedTodo={setEditedTodo} />;
  }

  function handleDelete() {
    /*eslint no-restricted-globals: ["error", "event", "fdescribe"]*/
    const isConfirmed = confirm(`Are you ready to remove "${todo.text}"`);

    if (!isConfirmed) return;
    dispatch(deleteTodo(todo.id));
  }

  function toggleComplete() {
    dispatch(completeTodo(todo.id));
  }

  return (
    <div key={todo.id} className={classes.join(' ')}>
      <div onClick={toggleComplete}>{todo.text}</div>
      <div className={todoItemClasess.icons}>
        <FiX onClick={handleDelete} />
        {!todo.isCompleted && (
          <FiEdit2
            onClick={() => setEditedTodo({ ...todo })}
            style={{ width: '20px' }}
          />
        )}
      </div>
    </div>
  );
}

// TodoItem.propTypes = {
//   todo: PropTypes.object.isRequired,
// };

TodoItem.propTypes = {
  todo: PropTypes.shape({
    id: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    isCompleted: PropTypes.bool.isRequired,
  }),
};
