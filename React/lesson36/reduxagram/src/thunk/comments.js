import { getComments } from '../api';
import { loadComments, failureComments } from '../actions/comments';

export const fetchComments = (id) => {
  return async (dispatch) => {
    try {
      const { data: comments } = await getComments(id);
      dispatch(loadComments(comments));
    } catch (e) {
      dispatch(failureComments(e));
    }
  };
};
