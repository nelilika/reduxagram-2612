import React, { useState, useContext } from 'react';
import { FiEdit2, FiX } from 'react-icons/fi';
import AddTodo from './AddTodo';
import { TodoContextComponent } from '../context/Context';
import { deleteTodo, completeTodo } from '../reducer/todoReducer';

export default function TodoList({ todo }) {
  const [editedTodo, setEditedTodo] = useState(null);
  const [, dispatch] = useContext(TodoContextComponent);
  const classes = ['todo-row'];

  if (todo.isCompleted) {
    classes.push('complete');
  }

  if (editedTodo) {
    return <AddTodo editedTodo={editedTodo} setEditedTodo={setEditedTodo} />;
  }

  function handleDelete() {
    /*eslint no-restricted-globals: ["error", "event", "fdescribe"]*/
    const isConfirmed = confirm(`Are you ready to remove "${todo.text}"`);

    if (!isConfirmed) return;
    dispatch(deleteTodo(todo.id));
  }

  function toggleComplete() {
    dispatch(completeTodo(todo.id));
  }

  return (
    <div key={todo.id} className={classes.join(' ')}>
      <div onClick={toggleComplete}>{todo.text}</div>
      <div className="icons">
        <FiX onClick={handleDelete} />
        {!todo.isCompleted && (
          <FiEdit2
            onClick={() => setEditedTodo({ ...todo })}
            style={{ width: '20px' }}
          />
        )}
      </div>
    </div>
  );
}
