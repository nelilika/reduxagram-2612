import { useState } from 'react';
import { useEffect } from 'react';
import NavBar from '../../components/UI/NavBar/NavBar';
import Card from '../../components/Card/Card';
import './TVShowInfo.scss';
import { useParams } from 'react-router-dom';

function TVShowInfo() {
  const [tvShow, setTvShow] = useState({});
  const { showId } = useParams();

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${showId}?api_key=5f06e2a08e0a49e1ebe93ef55540a01c`
    )
      .then((res) => res.json())
      .then((res) => setTvShow(res));
  }, []);

  return <Card card={tvShow} />;
}

export default TVShowInfo;
