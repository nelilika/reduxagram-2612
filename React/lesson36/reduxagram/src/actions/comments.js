export const LOAD_COMMENTS = '[COMMENTS] Load Comments';
export const FAILURE_COMMENTS = '[COMMENTS] Failure Comments';
export const ADD_COMMENT = '[COMMENTS] Add Comment';

export const loadComments = (comments) => ({
  type: LOAD_COMMENTS,
  payload: { comments },
});

export const failureComments = (error) => ({
  type: FAILURE_COMMENTS,
  payload: { error },
});

/**
 *
 * @param {Object} comment
 * @param {String} comment.user
 * @param {String} comment.text
 * @param {String} comment.postId
 * @returns
 */
export const addComment = (comment) => ({
  type: ADD_COMMENT,
  payload: { comment },
});
