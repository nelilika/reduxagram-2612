import React, { useContext, useMemo } from 'react';
import TodoItem from './TodoItem/TodoItem';
import { TodoContextComponent } from '../context/Context';

export default function TodoList() {
  const [{ todos }] = useContext(TodoContextComponent);

  const sortedTodos = useMemo(
    () => [...todos].sort((todo) => (todo.isCompleted ? 1 : -1)),
    [todos]
  );

  return (
    <div className="todo-list">
      {sortedTodos.map((todoItem) => (
        <TodoItem todo={todoItem} key={todoItem.id} />
      ))}
    </div>
  );
}
