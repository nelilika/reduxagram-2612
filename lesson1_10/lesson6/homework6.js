/* Завдання 2
Напишіть програму, яка питає у користувача число і виводить в консоль наступну інформацію про нього:
  - додатнє воно чи від'ємне;
  - скільки цифр в цьому числі;
  - якщо число додатне, виведіть суму його цифр

  Якщо number === 0, то виведе '0, lenght: 1'
  Якщо number === 100500, то виведе 'positive, length: 6, sum: 6'
  Если number === -50, то виведе 'negative, length: 2'
 */

{
  const userNumAsStr = prompt('Please enter an integer number:', '5');
  const userNumber = +userNumAsStr;
  let userNumSum = 0;

  if (
    isNaN(userNumber) ||
    !Number.isInteger(userNumber) ||
    isNaN(parseFloat(userNumAsStr))
  ) {
    console.error('Plese use only integer numbers!');
  } else {
    if (!userNumber) {
      console.log(`${userNumber}, length: ${userNumAsStr.length}`);
    } else if (userNumber > 0) {
      for (let i = 0; i < userNumAsStr.length; i++) {
        const userNumSymbol = +userNumAsStr.charAt(i);
        userNumSum += userNumSymbol;
      }
      console.log(
        `positive, length: ${userNumAsStrLength}, sum: ${userNumSum}`
      );
    } else {
      console.log(`negative, length: ${userNumAsStrLength - 1}`);
    }
  }
}

/* Завдання 3
Відомо, что подорож на Мальдіви коштує 3000$, а купити нові AirPods - 300$. 
Напишіть програму, яка питає у користувача число (в $) та виводить в консоль інформацію, 
  що він за ці гроші може купити.

  Якщо money === 200$, то виведе 'You can't do anything. I'm sorry :(';
  Якщо money === 300$, то виведе 'You can buy only AirPods';
  Якщо money === 3200.50$, то виведе 'You can go on vacation or buy AirPods! What are you waiting for?';
  Якщо money === 4300.53, то виведе 'You have enough money for everything. WOW!'
 */

{
  const journeyPrice = 3000;
  const airPodsPrice = 300;
  const allPurchasesPrice = journeyPrice + airPodsPrice;
  const userMoneySum = parseFloat(
    prompt('Please enter your sum of money in $:', '500$')
  );

  if (isNaN(userMoneySum) || userMoneySum < 0) {
    console.error('Please use 0 or positive numbers only!');
  } else {
    if (userMoneySum < airPodsPrice) {
      console.log("You can't do anything. I'm sorry :(");
    } else if (userMoneySum >= airPodsPrice && userMoneySum < journeyPrice) {
      console.log('You can buy only AirPods');
    } else if (
      userMoneySum >= journeyPrice &&
      userMoneySum < allPurchasesPrice
    ) {
      console.log(
        'You can go on vacation or buy AirPods! What are you waiting for?'
      );
    } else {
      console.log('You have enough money for everything. WOW!');
    }
  }
}

/* Завдання 5.
Напишіть програму, яка питає у користувача число

 від 1 до 9 і виводить таблицу множення для цього числа.

Якщо number === 5, то виведе
  1 x 5 = 5;
  2 x 5 = 10;
  3 x 5 = 15;
  ...
  9 x 5 = 45;
 upd: Виведіть всю таблицу множення.
 */
{
  let number /*= +prompt('Enter your number')*/;
  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    for (let i = 1; i <= 9; i++) {
      const result = `${i} x ${number} = ${i * number}`;
      console.log(result);
    }
  }
}
{
  for (let i = 1; i <= 9; i++) {
    for (let j = 1; j <= 9; j++) {
      const result = `${i} x ${j} = ${i * j}`;
      console.log(result);
    }
  }
}

/* Завдання 6
Напишіть програму, яка питає у користувача символ та число і виводить цей символ послідовно, 
збільшуючи кожен раз на 1, поки кількість символів в рядку не буде дорівнювати цьому числу.

  Якщо symbol === #, number === 5, то виведе
    #
    ##
    ###
    ####
    #####
  Якщо symbol === @, number === 6, то виведе
    @
    @@
    @@@
    @@@@
    @@@@@
    @@@@@@
 */

{
  const userSymbol = prompt('Please enter any symbol to repeat:', '*');
  const userNumOfRepeats = +prompt(
    'Please enter the number of symbol repeats:',
    '5'
  );
  let rowOfSymbols = '';

  if (
    isNaN(userNumOfRepeats) ||
    userNumOfRepeats <= 0 ||
    !Number.isInteger(userNumOfRepeats)
  ) {
    console.error('Please use positive integer numbers greater than 0!');
  } else {
    for (let i = 1; i <= userNumOfRepeats; i++) {
      rowOfSymbols += userSymbol;
      console.log(rowOfSymbols);
    }
  }
}

/* Завдання 9
Напишіть програму, яка питає у користувача число і виводить на экран ряд Фібоначі,
який має стільки чисел, скільки запросив користувач.
 */

{
  let number = +prompt('Enter your number');

  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    let first = 1;
    let second = 1;
    let third = 0;
    let fibonachi = `${first} ${second}`;

    for (let i = 0; i <= number; i++) {
      third = first + second;
      first = second;
      second = third;
      fibonachi += ` ${third}`;
    }

    console.log(fibonachi);
  }
}
{
  let number = +prompt('Enter your number');

  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    let fibonachi = '1 1';

    for (
      let i = 0, first = 1, second = 1, third = 1;
      i <= number;
      i++,
        third = first + second,
        first = second,
        second = third,
        fibonachi += ` ${third}`
    ) {}

    console.log(fibonachi);
  }
}

/* Завдання 10.
Напишіть програму, яка питає у користувача число (number) і підраховує максимальне число n ітерацій за формулою 1 + 2 + ... + n <= number.
  Якщо number === 5, то виведе 2 // 1 + 2 <= 5 
  Якщо number === 8, то виведе 3
 */

{
  let number = +prompt('Enter your number');

  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    let i = 1;
    let sum = 1;
    while (sum <= number) {
      // 1 <= 8 -> 3 <= 8 -> 6 <= 8
      sum += ++i; // 1 + (2) -> 3 + (3) -> 6 + (4)
    }

    console.log(i--);
  }
}
