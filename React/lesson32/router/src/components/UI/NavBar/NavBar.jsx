import classes from './NavBar.module.scss';
import { Link } from 'react-router-dom';

function NavBar() {
  return (
    <ul className={classes.menuBar}>
      <li>
        <Link to="/dashboard">Watch Now</Link>
      </li>
      <li>Movies</li>
      <li>
        <Link to="/dashboard/tv-shows">TV Shows</Link>
      </li>
      <li>Sports</li>
      <li>Kids</li>
      <li>Library</li>
    </ul>
  );
}

export default NavBar;
