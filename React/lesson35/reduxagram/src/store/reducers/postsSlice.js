import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getPosts } from '../../api';

export const fetchPosts = createAsyncThunk('posts/fetchPosts', (params) =>
  getPosts(params)
);

export const postsSlice = createSlice({
  name: 'posts',
  initialState: {
    posts: [],
    selectedPost: {}, // або можна зробити null
    error: null,
    totalCount: 0,
    limit: 5,
    loading: false,
    loaded: false,
  },
  reducers: {
    likePost: (state, action) => {
      state.posts = state.posts.map((post) =>
        post.id === action.payload
          ? {
              ...post,
              likes: ++post.likes,
            }
          : post
      );
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPosts.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.posts = action.payload.data;
        state.error = null;
        state.totalCount = action.payload.totalCount;
        state.loaded = true;
        state.loading = false;
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.posts = [];
        state.error = action.payload.error;
        state.loaded = true;
        state.loading = false;
      });
  },
});

export const { likePost } = postsSlice.actions;

export default postsSlice.reducer;

// мутуючі методи масиву

// .splice()
// [1,2,3].sort()
// .push()
// .pop()
// .unshift()
// .shift()
// [].fill()
// .reverse()

// // імутуючі методи масиву
// .filter()
// .map
// .slice()
// .
