// Scope
// Лексична область видимості
// Вкладені области видимості
// Пошук в області видимості
// Замикання

// Чим відрізняється блок коду від функції на практиці? (!)
// {
// let/const
// var
// }

// function() {
// }

function foo() {
  // global scope
  function boo() {
    // lexical scope
    function zoo() {
      console.log(value);
    }
    zoo();
  }
  boo();
}
// value is not defined

// Контекст/this
// let firstName = 'Gerald';

function getScriptName(firstName) {
  // let firstName = undefined, Gerald
  this.console.log(this);
  console.log('Hello, ' + firstName);
}

const getSurname = function (lastName) {
  console.log('Bye, ' + lastName);
};

getScriptName('Gerald'); // Hello, Gerald
// getName('Triss'); // Hello, Triss
// getName('Ciri'); // Hello, Ciri

// Метод обʼєкту
const student = {
  firstName: 'Gerald',
  lastName: 'is Rivii',
  age: 25,
  isMarried: false,
  getFullInfo(firstName, lastName, age) {
    console.log(this);
    console.log(`${firstName} ${lastName} - ${age} :(`);
  }, // метод обʼєкту - завжди функція
};

student.getFullInfo(); // undefined undefined - undefined :(

// Варіант 2
const studentWithThis = {
  firstName: 'Gerald',
  lastName: 'is Rivii',
  age: 25,
  isMarried: false,
  getFullInfo() {
    console.log(this);
    console.log(`${this.firstName} ${this.lastName} - ${this.age} :)`);
  }, // метод обʼєкту - завжди функція
};

studentWithThis.getFullInfo(); // 'Gerald is Rivii - 25 :)'

// Варіант 3

// bind, apply, call - методи функції

// bind - створює і повертає функцію (!) з новим контекстом
// call/apply - викликають функцію з новим контекстом
function getFullInfo(gender, weapons) {
  console.log(this);
  console.log(`${this.firstName} ${this.lastName} - ${this.age} :)`);
  console.log(gender);
  console.warn(weapons);
}

const studentGerald = {
  firstName: 'Gerald',
  lastName: 'is Rivii',
  age: 25,
  isMarried: false,
};

const studentTriss = {
  firstName: 'Triss',
  lastName: 'Merigold',
  age: 30,
  isMarried: true,
};

getFullInfo('other'); // виклик функції з контекстом window
const bindGeraldGetFullInfo = getFullInfo.bind(studentGerald, 'male'); // виклик методу bind, який створює і повертає функцію (!) з новим контекстом
bindGeraldGetFullInfo(); // виклик функції з новим контекстом studentGerald

getFullInfo('other'); // виклик функції з контекстом window
const bindTrissGetFullInfo = getFullInfo.bind(studentTriss, 'female'); // виклик методу bind, який створює і повертає функцію (!) з новим контекстом
bindTrissGetFullInfo(); // виклик функції з новим контекстом studentGerald

// apply
getFullInfo.apply(studentGerald, ['male', ['silver', 'aluminium']]); // аргументи функції передаються масивом
// call
getFullInfo.call(studentTriss, 'female', ['silver', 'aluminium']); // аргументи функції передаються через кому

// rest operator (...)
function func(...args) {}

func(1, 2, 3, 4, 5, 6);

console.log(Math.max(...[1, 24, 3])); // Math.max(1,24,3)
console.log(Math.max.apply(null, [1, 24, 3])); // Math.max(1,24,3)
// max(1,24,3) {} -> max([1, 24, 3]) {}

console.error('SPLIT');

bindGeraldGetFullInfo.apply(studentTriss); // apply не викличеться з новим контекстом
// бо функція може забайндіти тільки один контекст на завжди

console.log(this);

// eventLoop
// втрата контексту - setTimeout(), setInterval(),
