import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

const logger = createLogger({});

const middlewares = [thunk];

console.log(process.env.NODE_ENV);

if (process.env.NODE_ENV === 'development') {
  middlewares.push(logger);
}

export { middlewares };
