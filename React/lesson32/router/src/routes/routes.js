import { Navigate, createBrowserRouter } from 'react-router-dom';
import Dashboard from '../pages/Dashboard/Dashboard';
import Login from '../pages/Login/Login';
import TVShows from '../pages/TVShows/TVShows';
import TVShowInfo from '../pages/TVShowInfo/TVShowInfo';
import ProtectedRoute from './ProtectedRoute';

export const router = createBrowserRouter([
  {
    path: '/dashboard',
    element: (
      <ProtectedRoute>
        <Dashboard />
      </ProtectedRoute>
    ),
    children: [
      {
        path: 'tv-shows',
        element: <TVShows />,
      },
      {
        path: 'tv-show/:showId',
        element: <TVShowInfo />,
      },
      {
        path: '*',
        element: <h1>Not found</h1>,
      },
    ],
  },
  {
    path: 'login',
    element: <Login />,
  },
  {
    path: '*',
    element: <Navigate to="dashboard" />,
  },
]);
