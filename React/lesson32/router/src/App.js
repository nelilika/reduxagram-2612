import * as React from 'react';
import { RouterProvider } from 'react-router-dom';
import { router } from './routes/routes';

export default function App() {
  return (
    <RouterProvider router={router} />
    // <BrowserRouter>
    //   <Routes>
    //     <Route
    //       path="dashboard"
    //       element={
    //         <ProtectedRoute>
    //           <Dashboard />
    //         </ProtectedRoute>
    //       }
    //     >
    //       <Route path="tv-shows" element={<TVShows />} />
    //       <Route path="tv-show/:showId" element={<TVShowInfo />} />
    //       <Route path="*" element={<h1>Not found</h1>} />
    //     </Route>
    //     <Route path="login" element={<Login />} />
    //     <Route path="*" element={<Navigate to="dashboard" />} />
    //   </Routes>
    // </BrowserRouter>
  );
}
