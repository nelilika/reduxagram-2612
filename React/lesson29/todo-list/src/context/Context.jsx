import { createContext, useReducer } from 'react';
import { todoReducer, initialState } from '../reducer/todoReducer';

export const TodoContextComponent = createContext({});

export default function TodoContext({ children }) {
  const [state, dispatch] = useReducer(todoReducer, initialState); // дуже схожа поведінка на хук useState

  return (
    <TodoContextComponent.Provider value={[state, dispatch]}>
      {children}
    </TodoContextComponent.Provider>
  );
}
