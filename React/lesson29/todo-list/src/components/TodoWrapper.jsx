import { useState, useEffect, useContext } from 'react';
import '../App.scss';
import AddTodo from './AddTodo';
import TodoList from './TodoList';
import { getTodo } from '../server/api';
import { TodoContextComponent } from '../context/Context';
import { loadTodo } from '../reducer/todoReducer';

function TodoWrapper() {
  const [, dispatch] = useContext(TodoContextComponent);
  let [counter, setCounter] = useState(0);

  useEffect(() => {
    getTodo().then((todos) => dispatch(loadTodo(todos)));
  }, []);

  return (
    <div className="todo-app">
      <h1 onClick={() => setCounter(counter++)}>
        What's plans for today, bro? {counter}
      </h1>
      <AddTodo />
      <TodoList />
    </div>
  );
}

export default TodoWrapper;
