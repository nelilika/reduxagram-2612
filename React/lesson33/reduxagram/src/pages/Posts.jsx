import React, { useEffect } from 'react';
import PostGrid from '../components/Posts/PostGrid';
import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import { getPosts } from '../api';
import { loadPosts } from '../actions/posts';

export default function Posts() {
  // const [state, dispatch] = useContext(ReduxagramContext);
  const dispatch = useDispatch();

  useEffect(() => {
    getPosts({ limit: 10, page: 1 }).then((posts) =>
      dispatch(loadPosts(posts))
    );
  }, [dispatch]);

  return (
    <>
      <Typography
        variant="h1"
        component="h2"
        sx={{
          textAlign: 'center',
          fontFamily: 'Festive',
          color: 'lightBlue',
          m: '15px 0',
        }}
      >
        Reduxagram
      </Typography>
      <PostGrid />
    </>
  );
}
