import { Component, createRef } from 'react';

const ROW_HEIGHT = 10;

class ImageItem extends Component {
  constructor() {
    super();

    this.imageRef = createRef();

    this.state = {
      span: 0,
    };
  }

  componentDidMount() {
    const imageDOMElement = this.imageRef.current;
    console.log(imageDOMElement);
    console.log(this.imageRef.current.clientHeight);

    imageDOMElement.addEventListener('load', () => {
      console.log(this.imageRef.current.clientHeight);
      const height = this.imageRef.current.clientHeight;
      this.setState({ span: Math.ceil(height / ROW_HEIGHT) });
    });
  }

  render() {
    const {
      alt_description: alt,
      urls: { regular: src }, // const src = this.props.image.urls.regular;
    } = this.props.image;

    return (
      <img
        src={src}
        alt={alt}
        style={{ gridRowEnd: `span ${this.state.span}` }}
        ref={this.imageRef}
      />
    );
  }
}

export default ImageItem;
