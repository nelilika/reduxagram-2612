// Обʼекти, масиви

let dymamicValue = "Hello, i'm outside of object";

// новий обʼект
const student = {
  firstName: 'John',
  lastName: 'Smith',
  age: 22,
  country: 'Ukraine',
  education: 'no',
  isMarried: true,
  notes: null,
  marks: {
    math: 23,
    english: 100,
  },
  ' gh hj': 23, // будь-ласка так не робіть
  2: 2,
  dymamicValue: 'Random value',
  [dymamicValue]: 'Random dynamic value',
};
// ключі обʼекта firstName, lastName, age, country ... завжди мають тип даних String
console.log(student);

console.log(student.lastName); // Smith
console.log(student.marks.english); // 100;
console.log(student[' gh hj']); // 23
console.log(student['2']); // 2
console.log(student[dymamicValue]); // Random dynamic value
console.log(student.dymamicValue); // Random value

// in
console.log('age' in student); // true
console.log('hello' in student); // false

// undefined і додавання значеннь
console.log(student.hello); // let hello;
student.hello = 'hello';
console.log(student.hello); // 'hello' - typeof string

student.hello = 'hello 2';

// видалення значень і ключів
delete student.hello;
console.log(student);

// спеціальний цикл для обʼекту for ... in

for (let key in student) {
  console.log(key);
  console.log(student[key]);
  if (student[key] === 22) {
    console.error(key);
  }
}

for (let key in student.marks) {
  console.log(key);
  console.log(student.marks[key]);
}

// Порівняння обʼєктів
let number1 = 1;
let number2 = 1;
console.log(number1 === number2); // true

let string1 = 'qwerty';
let string2 = 'qwerty';
console.log(string1 === string2); // true

console.log(true === true); // true

const obj1 = {};
const obj2 = {};
console.log(obj1 === obj2); // false
// обʼєкт НІКОЛИ не дорівнює іншому обʼєкту окрім випадку в рядку 81

const obj3 = {};
const obj4 = obj3;
console.log(obj3 === obj4); // true

// Массиви
const array = [];
const arrayNumbers = [1, 2, 3, 4, 56];
const arrayStrings = ['qwerty', 'ytrewq', 'asdfgh', 'hgfdsa'];
const arrayAnything = [1, '1', true, {}, [], undefined, null]; // зазвичай так ніхто не пише
console.log(typeof array);

for (let key in arrayStrings) {
  console.log(arrayStrings[key]);
}

for (let i = 0; i < arrayStrings.length; i++) {
  console.log(arrayStrings[i]); // arrayStrings[0], arrayStrings[1]
}

// Чому обʼекти ніколи не рівні один одному?
{
  const object1 = {}; // задаємо посилання на місце в памʼяті, де цей обʼект створено
  object1.name = 'name';

  const object2 = {}; // задаємо посилання на місце в памʼяті, де цей обʼект створено
  console.log(object1 === object2); // false

  const object3 = {}; // задаємо посилання на місце в памʼяті, де цей обʼект створено
  const object4 = object3; // копіємо посилання на місце в памʼяті
  console.log(object3 === object4); // true

  // значимьіе типьі данньіх (value data type) - всі примітиви
  // cсьіллочньій типьі данньіх (reference data type) - Object
}

{
  const object1 = {
    a: 1,
  };

  const object2 = object1; // копіювання за посиланням

  object2.b = 'b';

  console.log(object1);
  console.log(object2);
}

// Копіювання обʼєкт
// спред опертор '...'

const student2 = {
  ...student, // копіювання за значенням
};
// спред оператор працює тільки з перший рівень вкладення
student2.firstName = 'Anna';
student2.lastName = 'Kovalska';
student2.marks.english = 0; // в цьому випадку оцінка змінеться і у student

{
  student.marks.english = 100;
  const student2 = {
    ...student, // копіювання за значенням
    marks: {
      ...student.marks, // копіювання за значенням
    },
  };
  student2.firstName = 'Anna';
  student2.lastName = 'Kovalska';
  student2.marks.english = 0; // в цьому випадку оцінка змінеться ТІЛЬКИ У student

  console.log(student);
  console.log(student2);
}

// JSON.parse(); // String -> Object
// JSON.stringify(); // Object -> String

// Глубокое копирование за допомогою JSON
const studentDeep = JSON.parse(JSON.stringify(student));
