import { getComments } from '../api';
import { loadComments, failureComments } from '../actions/comments';

export const fetchComments = (id) => {
  return async (dispatch) => {
    try {
      const posts = await getComments(id);
      dispatch(loadComments(posts));
    } catch (e) {
      dispatch(failureComments(e));
    }
  };
};
