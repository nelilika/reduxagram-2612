import { LOAD_POSTS } from '../actions/posts';

// початковий стейт
export const initialState = {
  posts: [],
  selectedPost: {}, // або можна зробити null
};

// редьюсер
export const postsReducer = function (state = initialState, action) {
  switch (action.type) {
    case LOAD_POSTS:
      return {
        ...state,
        posts: [...action.payload.posts],
      };
    default:
      return state;
  }
};
