// Http request example
// Method GET

fetch('https://jsonplaceholder.typicode.com/posts')
  .then((res) => {
    console.log(res);
    //return undefined;
    return res.json();
  })
  .then((res) => console.log(res));

// Method POST
fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST', // *GET, POST, PUT, DELETE, etc.
  mode: 'cors', // no-cors, *cors, same-origin
  cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
  credentials: 'same-origin', // include, *same-origin, omit
  headers: {
    'Content-Type': 'application/json',
    // 'Content-Type': 'application/x-www-form-urlencoded',
  },
  redirect: 'follow', // manual, *follow, error
  referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  body: JSON.stringify({
    text: 'Some text in post',
  }), // body data type must match "Content-Type" header
})
  .then((res) => {
    console.log(res);
    //return undefined;
    return res.json();
  })
  .then((res) => console.log(res));

// async / await

async function getName() {
  // return Promise.resolve('Aurora');
  return 'Aurora'; // resolve data
}

getName().then((res) => {
  console.log('Hello, dear ' + res);
});

// const name = await getName(); // працює виключно в функції створеної за допомогою async - код поверне помилку
// console.log(name);
// console.log('Hello, dear ' + )

async function start() {
  const name = await getName();
  console.log('Hello, dear ' + name);

  const rawData = await fetch('https://jsonplaceholder.typicode.com/posts');
  const posts = await rawData.json();
  // const posts3 = await (
  //   await fetch('https://jsonplaceholder.typicode.com/posts')
  // ).json();
  console.log(posts);
}

start();
