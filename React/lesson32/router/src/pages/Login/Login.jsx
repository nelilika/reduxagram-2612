import './Login.scss';
import { useNavigate } from 'react-router-dom';

function Login() {
  const navigate = useNavigate();

  function handleSubmit(e) {
    e.preventDefault();

    localStorage.setItem('token', 'some-token');
    // dispatch(loginUser(user))
    navigate('/dashboard');
  }

  return (
    <div className="login-wrapper">
      <div className="login-box">
        <h2>Login</h2>
        <form onSubmit={handleSubmit}>
          <div className="user-box">
            <input type="text" name="" required="" />
            <label>Username</label>
          </div>
          <div className="user-box">
            <input type="password" name="" required="" />
            <label>Password</label>
          </div>
          <button className="submit-btn">
            <a>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              Submit
            </a>
          </button>
        </form>
      </div>
    </div>
  );
}

export default Login;
