import { combineReducers } from 'redux';
import postsReducer from './postsSlice';
import commentsReducer from './commentsSlice';

export const rootReducer = combineReducers({
  postsReducer,
  commentsReducer,
});
