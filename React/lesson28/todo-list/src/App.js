import { useState, useEffect } from 'react';
import './App.scss';
import AddTodo from './components/AddTodo';
import TodoList from './components/TodoList';
import { getTodo } from './server/api';

//@TODO: different title depends on time

function App() {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    getTodo().then((todoArr) => setTodos(todoArr));
  }, []);

  function deleteTodo(todoId) {
    setTodos(todos.filter((todoItem) => todoItem.id !== todoId));
  }

  function addNewTodo(todo) {
    setTodos([...todos, todo]);
  }

  function toggleCompletedTodo(todoId) {
    const newTodos = todos.map((todoItem) => {
      return todoItem.id === todoId
        ? {
            ...todoItem,
            isCompleted: !todoItem.isCompleted,
          }
        : todoItem;
    });
    setTodos(newTodos);
  }

  function updateTodo(updatedTodo) {
    const updatedTodos = todos.map((todo) =>
      todo.id === updatedTodo.id ? updatedTodo : todo
    );
    setTodos(updatedTodos);
  }

  useEffect(() => {
    console.log(todos);
    return () => console.log('removed');
  }, [todos]);

  return (
    <div className="todo-app">
      <h1>What's plans for today, bro?</h1>
      <AddTodo addNewTodo={addNewTodo} />
      <TodoList
        todos={todos}
        deleteTodo={deleteTodo}
        toggleCompletedTodo={toggleCompletedTodo}
        updateTodo={updateTodo}
      />
    </div>
  );
}

export default App;
