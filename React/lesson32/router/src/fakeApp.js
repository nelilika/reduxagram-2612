import * as React from 'react';
import { Routes, Route, Outlet, Link, BrowserRouter } from 'react-router-dom';
import Dashboard from './pages/Dashboard/Dashboard';
import Login from './pages/Login/Login';
import TVShows from './pages/TVShows/TVShows';

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        {/** http://localhost:3000/dashboard */}
        <Route path="/dashboard" element={<Layout />}>
          <Route index element={<Home />} />
          {/** http://localhost:3000/dashboard/about */}
          <Route path="about" element={<About />} />
          <Route path="dashboard" element={<Dashboard />} />
          <Route path="*" element={<NoMatch />} />
        </Route>
        <Route path="login" element={<Login />} />
      </Routes>
      {/* <Routes>
        <Route path="/home" element={<Home />}></Route>
        <Route path="/dashboard" element={<Dashboard />}></Route>
        <Route path="/about" element={<About />} />
        <Route path="/login" element={<Login />} />
        <Route path="/tv-shows" element={<TVShows />} />
        <Route path="*" element={<NoMatch />} />
      </Routes> */}
    </BrowserRouter>
  );
}

function Layout() {
  return (
    <div>
      <nav>
        <ul>
          <li>
            <Link to="/dashboard">Home</Link>
          </li>
          <li>
            <Link to="/dashboard/about">About</Link>
          </li>
          <li>
            <Link to="/dashboard/dashboard">Dashboard</Link>
          </li>
          <li>
            <Link to="/nothing-here">Nothing Here</Link>
          </li>
        </ul>
      </nav>

      <hr />
      <Outlet />
      <footer>
        <h1>This is the end of the page</h1>
      </footer>
    </div>
  );
}

function Home() {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}

function About() {
  return (
    <div>
      <h2>About</h2>
    </div>
  );
}

function NoMatch() {
  return (
    <div>
      <h2>Nothing to see here!</h2>
      <p>
        <Link to="/dashboard">Go to the home page</Link>
      </p>
    </div>
  );
}
