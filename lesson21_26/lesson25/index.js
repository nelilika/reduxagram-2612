const express = require('express');
// const aliens = require('./aliens.json');
const fs = require('fs');
const util = require('util');
const bodyParser = require('body-parser');

const app = express();
const PORT = 3000;
const ERROR_CODE_400 = 400;
const ERROR_CODE_500 = 500;

const readFileAsPromise = util.promisify(fs.readFile);
const writeFileAsPromise = util.promisify(fs.writeFile);

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(error);

app.get('/', (req, res) => {
  res.send('Hello World!');
});

// CRUD (create, read, update, delete)
// READ
app.get('/aliens', async (req, res) => {
  // сінхронний код - приклад поганого коду
  // const aliens = fs.readFileSync('./aliens.json', 'utf-8');

  // асінхронний код за допомогою колбеку
  // fs.readFile('./aliens.json', 'utf-8', (err, aliens) => {
  //   if (err) {
  //     throw err;
  //   }
  //   res.send(aliens);
  // });

  // асінхронний код за допомогою промісів
  // readFileAsPromise('./aliens.json', 'utf-8')
  //   .then((aliens) => res.send(aliens))
  //   .catch((err) => {
  //     throw err;
  //   });

  // асінхронний код за допомогою async/await
  const aliens = await readFileAsPromise('./aliens.json', 'utf-8');
  res.send(aliens);
});

// CREATE
app.post('/aliens', async (req, res) => {
  if (req.body.id === '1234') {
    res.status(ERROR_CODE_400);
    res.send('Bad Reqest');
  }
  console.log(req.body);
  await writeFileAsPromise('./random.json', JSON.stringify(req.body));
  res.status(201);
  res.send('Success');
});

// UPDATE
app.put('/aliens/:id', async (req, res) => {
  // req.body
  // update random.json
  const random = JSON.parse(await readFileAsPromise('./random.json', 'utf-8'));

  const newObject = Object.assign({}, random, req.body);
  await writeFileAsPromise('./random.json', JSON.stringify(newObject));
  res.send('Updated successfully');
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});

function error(err, req, res, next) {
  console.log(err);
  if (err.message === ERROR_CODE_400) {
    res.status(ERROR_CODE_400);
    res.send('Bad Reqest');
  }

  res.status(ERROR_CODE_500);
  res.send('Internal Server Error');
}
