// Функція-конструктор
function CityFunction(name, population, isCapital) {
  // this = {}
  this.name = name;
  this.population = population;
  this.isCapital = isCapital;
  // return this;
}

CityFunction.prototype.logInfo = function () {
  return `Hello from ${this.name}. There are ${this.population} people living here!`;
};

{
  const city1 = new CityFunction('Kharkiv', 1.5e6, false);
  const city2 = new CityFunction('Kiyv', 3e6, true);
  const city3 = new CityFunction('Chornomorsk', 8e4, false);
  console.log(city1);
  console.log(city2.logInfo());
  console.log(city3);

  for (let key in city1) {
    console.log(key);
    // (!) цикл for ..in проходиться по всім ключам обʼєкта і по всіх ключах його прототипа!
    if (city1.hasOwnProperty(key)) {
      console.warn('only in object city1', key);
    }
  }

  for (let key of Object.keys(city1)) {
    console.error(key);
  }
}
/**
 * {
 *   name: 'Kharkiv',
 *   population: 1500000,
 *   isCapital: false,
 *   logInfo: function() {} АБО
 *   logInfo() {}
 * }
 */

// Клас - recomended to use

class City {
  #counter = 45; // приватна властивість
  constructor(name, population, isCapital) {
    // this
    this.name = name;
    this.population = population;
    this.isCapital = isCapital;
  }

  get counter() {
    // можливіть отримати значення приватного методу - геттер
    return this.#counter;
  }

  set counter(value) {
    // можливіть змінити значення приватного методу - сеттер
    this.#counter = value;
  }

  publicLogInfo() {
    // публічний метод
    this.#counter = 50;
    return this.#logInfo();
  }

  #logInfo() {
    // приватний метод
    return `Hello from ${this.name}. There are ${this.population} people living here!`;
  }
}

class District extends City {
  constructor(name, population) {
    // this створюватись не буде
    super(); // наслідує від батьківського класу this
    this.name = name; // super === this
    this.population = population;
  }
  // наслідується і доступний геттер і сеттер City
  // наслідується метод publicLogInfo тільки
}

const city1 = new City('Kharkiv', 1.5e6, false);
const city2 = new City('Kiyv', 3e6, true);
// City.prototype === city2.__proto__
const city3 = new City('Chornomorsk', 8e4, false);
console.log(city1);
console.log(city2);
console.log(city3);

for (let key in city3) {
  console.log(key);
}

const district = new District('Moskaliovka', 7e3);
console.log(district);

// приклад наслідування. Цей метод існує, бо написаний у батьківського класа
district.publicLogInfo();
// district.#counter; - Error не можемо звертатись до приватної властивості

// Прототипи (прототипне наслідування)

// null / undefined  - не мають прототипного наслідування

// тип number

const cities = ['Kiyv', 'London', 'Helsinki'];
// cities[[Prototype]] - приватна властивіть, яка показує посилання на конструктор батьківського класу
// але недоступна для читання і редагування

// Array.prototype === cities[[Prototype]]; // true
// Array.prototype === cities.__proto__; // true

// Приклади нативних функція-конструкторів в JavaScript
// Number, String, Boolean, Array, Object, RegExp, Function, Date, Error, Promise, Map, Set

// Array.prototype.constructor - саме тут створюється контекст для майбутніх екземплярів обʼєкту

// стрілочка функція не має свого контексту і властивості prototype

// властивість prototype є у КОЖНОЇ функції створеної за допомогою ключового слова function

const getName = () => {
  console.log('Some name');
};
// new getName() - буде помилка

const getSurname = function () {
  console.log('Some surname');
}; // у анонімної функції є prototype

// Object.defineProperty()

const student = {
  name: 'Anna',
  surname: 'Kowalski',
  age: 25,
};

Object.defineProperty(student, 'isLeader', {
  value: true,
  writable: true, // чи можна змінювати значення чи ні, типу const isLeader = true,
  enumerable: true, // чи буде перераховуватись в циклі чи ні
  configurable: false, // чи можна видалити властивість
});

console.log(student);
