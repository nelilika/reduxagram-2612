// Інші види функції

// 1) функція-рекурсія
// 2) функція вищого порядку
// 3) функція планування
// 4) функція самовикликаюча

// 4) функція самовикликаюча
// IIFE - deprecated
(function () {
  console.log('Hello from IIFE');
})();

// 3) функція планування
console.log('Hello right now');

function logMessage() {
  console.log('Hello in 5 seconds');
}

logMessage();

setTimeout(logMessage, 5000);
console.log('Hello after setTimeout function');

const intervalId = setInterval(() => {
  console.log('im the part of the interval');
}, 3000);
console.log(intervalId);

// clearInterval(intervalId); припиняє роботу функції setInterval()
// clearTimeout(); припиняє роботу функції setTimeout()

setTimeout(function () {
  clearInterval(intervalId);
}, 12000);

// 2) функція вищого порядку
function someFunc() {
  function logInternalMessage() {
    console.log('hello from internal function');
  }

  return logInternalMessage;
}

const func = someFunc();
func();

// someFunc - фунція вищого порядку
// logInternalMessage - function declaration
// func - function expression

// 1) функція callback()
function randomFunc(callbackFn, someValue) {
  callbackFn(); // виклик функції
}

// 0) функція рекурсія
function test(number) {
  // console.log('before');
  if (number === 0) {
    // console.log('return');
    return;
  }
  test(number - 1);
}

test(1);

// сумма всіх чисел sum-n = n * (n + 1) / 2
// факторіал: n! = (n) * (n-1)!
// експонента

// function factorial(number) {
//   // console.log('before', number);
//   if (number === 0) {
//     // console.log('return');
//     return 1;
//   }
//   return number * factorial(number - 1);
// }

const factorial = (number) =>
  number !== 0 ? number * factorial(number - 1) : 1;

console.time('recuise');
console.log(factorial(46));
console.timeEnd('recuise');

console.time('forF');
let factorialN = 1;
for (let n = 46; n > 0; n--) {
  factorialN = n * factorialN;
}
console.log(factorialN);
console.timeEnd('forF');

// Методи масивів і рядків

// let array = new Array(10); - створили ПОРОЖНІЙ массив [empty x 10].
// З empty не працюються методи масиву
// array = array.fill(null); - заповнили масив

console.time('methods');
const array = new Array(10).fill(null).map(() => {
  return getRandomInt(1, 10);
});
console.timeEnd('methods');
console.log(array);

// console.time('for');
// for (let i = 0; i < array.length; i++) {
//   array[i] = getRandomInt(1, 10);
// }
// console.timeEnd('for');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}

console.log(array);

// Array.forEach() - просто перебирає масив
// це аналог методів for, do, while
let sum = 0;
array.forEach(function (item, index, array) {
  sum += item;
});
console.log(sum);

console.time('Check time');
for (let i = 0; i < 1e8; i++) {
  // console.log(i);
}
console.timeEnd('Check time');

// for, do, while, Array.forEach - *не рекомендую

// Array.filter() - фільтрує дані

const filteredArray = array.filter((item) => {
  console.log('hello from the loop');
  return !(item % 2); // умова, де якщо true, то елемент залишиться, а якщо false відкинеться
});
console.log(filteredArray);

function funcWithReturn() {
  return 4;
}

function funcWithoutReturn() {
  let sum = 4;
}

console.log(funcWithReturn()); // 4
console.log(funcWithoutReturn()); // undefined

// Array.map() - проходиться по масиву і змінює його значення згідно з тим значенням,
// яке поверне функція-колбек

// не рекомендую
// Array.push(), Array.pop() - додає в кінець, видаляе останній елемент массиву
// Array.unshift(), Array.shift() - додає на початок, видаляє перший

const arrayPush = [1, 2, 3, 4];
arrayPush.push(5); // модифікує масив

// рекоменду
// slice() - не модифікує масив - повертає часткову копію масиву (тобто можна видаляти елементи )
// splice() - модифікує масив - видаляє або додає значення в масив

// reduce()
const reducedSum = array.reduce(function (sum, item) {
  sum += item;
  return sum;
});
console.log(reducedSum);
