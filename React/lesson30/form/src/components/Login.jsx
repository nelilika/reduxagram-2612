import React from 'react';
import Button from '@mui/material/Button';
import FormInput from './UI/Input';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

const schema = yup
  .object({
    email: yup.string().nullable().email('Hey, please check your email'),
    password: yup.string().required('Dont forget about typing password'),
  })
  .required();

const Login = ({ setIsLoginPage }) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  // console.log(watch('email'));

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <>
      <form noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
        <h2>Login Form</h2>
        {/* <input {...register('example')} /> */}
        <FormInput
          label="Email"
          {...register('email')}
          type="email"
          sx={{ mb: 3 }}
        />
        <p>{errors.email?.message}</p>
        <FormInput
          label="Password"
          {...register('password')}
          type="password"
          sx={{ mb: 3 }}
        />
        <p>{errors.password?.message}</p>
        <Button variant="outlined" color="secondary" type="submit">
          Login
        </Button>
      </form>
      <small>
        Need an account?{' '}
        <a href="/#" onClick={() => setIsLoginPage(false)}>
          Register here
        </a>
      </small>
    </>
  );
};

export default Login;
