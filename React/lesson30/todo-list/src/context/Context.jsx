import { createContext, useReducer } from 'react';
import { todoReducer, initialState } from '../reducer/todoReducer';

export const TodoContext = createContext({});

export default function TodoProvider({ children }) {
  // TodoContextComponent
  const [state, dispatch] = useReducer(todoReducer, initialState); // дуже схожа поведінка на хук useState

  return (
    <TodoContext.Provider value={[state, dispatch]}>
      {children}
    </TodoContext.Provider>
  );
}
