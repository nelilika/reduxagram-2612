'use strict';

// const protected = 'qwerty';
// console.log(protected); // error

console.log(this); // window

function someFunc() {
  console.log(this); // window -> undefined
  console.log(this.firstname);
}
