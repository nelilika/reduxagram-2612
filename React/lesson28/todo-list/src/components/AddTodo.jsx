import { useRef, useEffect } from 'react';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

function AddTodo(props) {
  const { addNewTodo, editedTodo, updateTodo, setEditedTodo } = props;
  console.log(props);
  const [newTodo, setNewTodo] = useState(editedTodo ? editedTodo.text : '');
  const inputRef = useRef('');
  console.log(inputRef);

  const onChange = function (event) {
    setNewTodo(event.target.value);
  };

  // componentDidMount
  useEffect(() => {
    console.log('componentDidMount');
  }, []);

  // componentDidUpdate
  useEffect(() => {
    console.log(inputRef);
    console.log('componentDidUpdate');
    inputRef.current.focus();
  }, [inputRef]);

  const onSubmit = function (event) {
    event.preventDefault();

    if (editedTodo) {
      const todo = {
        ...editedTodo,
        text: newTodo,
      };
      updateTodo(todo);
      setEditedTodo(null);
    } else {
      const todo = {
        id: uuidv4(),
        text: newTodo,
        isCompleted: false,
      };
      addNewTodo(todo);
    }
    setNewTodo('');
  };
  console.log(editedTodo);
  return (
    <form className="todo-form" onSubmit={onSubmit}>
      <input
        value={newTodo}
        onChange={onChange}
        className="todo-input"
        placeholder="Add a todo"
        ref={inputRef}
      />
      <button className="todo-button">
        {editedTodo ? 'Update' : 'Add Todo'}
      </button>
    </form>
  );
}

export default AddTodo;
