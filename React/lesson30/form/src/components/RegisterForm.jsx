import React, { useState, createRef } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import FormInput from './UI/Input';

const schema = yup
  .object({
    firstName: yup.string().required(),
    lastName: yup.string().required(),
    email: yup.string().nullable().email('Hey, please check your email'),
    password: yup.string().required('Dont forget about typing password'),
    birthDay: yup.string().required(),
  })
  .required();

const RegisterForm = ({ setIsLoginPage }) => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const fileInputRef = createRef();
  const [image, setImage] = useState({
    src: '',
    name: '',
  });

  function onSubmit(data) {
    console.log(data);
  }

  function updateImage(event) {
    console.log(event); // <- прийде файл, який ви обрали

    const reader = new FileReader();
    const file = event.target.files[0];
    console.log(file);
    console.log(reader);

    if (file) {
      reader.readAsDataURL(file); // конвертує ваш файл base64
    }

    reader.onloadend = () => {
      console.log(reader.result);
      setImage({
        src: reader.result,
        name: file.name,
      });
    };
  }

  function choseFile() {
    fileInputRef.current.click();
  }

  return (
    <React.Fragment>
      <h2>Register Form</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Stack spacing={2} direction="row" sx={{ marginBottom: 4 }}>
          <FormInput
            label="First Name"
            {...register('firstName')}
            type="text"
          />
          <FormInput label="Last Name" {...register('lastName')} type="text" />
        </Stack>
        <FormInput
          label="Email"
          {...register('email')}
          type="email"
          sx={{ mb: 3 }}
        />
        <FormInput
          label="Password"
          {...register('password')}
          type="password"
          sx={{ mb: 3 }}
        />
        <FormInput
          label="Date of Birth"
          {...register('birthDay')}
          type="date"
          sx={{ mb: 3 }}
        />
        <FormLabel id="demo-radio-buttons-group-label">Gender</FormLabel>
        <RadioGroup
          aria-labelledby="demo-radio-buttons-group-label"
          defaultValue="female"
          name="radio-buttons-group"
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <FormControlLabel
            value="female"
            control={<Radio {...register('gender')} />}
            label="Female"
          />
          <FormControlLabel
            value="male"
            control={<Radio {...register('gender')} />}
            label="Male"
          />
          <FormControlLabel
            value="other"
            control={<Radio {...register('gender')} />}
            label="Other"
          />
        </RadioGroup>

        <Button fullWidth sx={{ marginBottom: '10px' }} onClick={choseFile}>
          Choose File
        </Button>
        <label>
          <TextField
            sx={{ display: 'none' }}
            type="file"
            {...register('avatar')}
            onChange={updateImage}
            ref={fileInputRef}
          />
        </label>
        <img style={{ maxWidth: 400 }} src={image.src} alt={image.name} />

        <Button variant="outlined" color="secondary" type="submit">
          Register
        </Button>
      </form>
      <small>
        Already have an account?{' '}
        <a href="/#" onClick={() => setIsLoginPage(true)}>
          Login Here
        </a>
      </small>
    </React.Fragment>
  );
};

export default RegisterForm;
