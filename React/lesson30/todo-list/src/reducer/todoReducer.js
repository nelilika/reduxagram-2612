/**
 * @typedef {Object} Action
 * @property {string} type
 * @property {*} payload
 */

export const initialState = {
  todos: [],
  initialTodos: [],
};

// action's types
const LOAD_TODO = '[TODO] Load Todo';
const ADD_TODO = '[TODO] Add Todo';
const UPDATE_TODO = '[TODO] Update Todo';
const DELETE_TODO = '[TODO] Delete Todo';
const COMPLETE_TODO = '[TODO] Complete Todo';
const FILTER_COMPLETED_TODO = '[Todo] Filter Completed Todo';
const CLEAR_FILTERS = '[Todo] Clear Filters';
const SEARCH_BY_NAME = '[Todo] Search By Name';

// actios
/**
 * @param {Array} todos - array todos
 * @returns {Action} action
 */
export const loadTodo = (todos) => ({
  type: LOAD_TODO,
  payload: { todos },
});

/**
 * @param {Object} todo - new todo
 * @param {number} todo.id - id todo
 * @param {string} todo.text - todo title
 * @param {boolean} todo.isCompleted - toggle to check if value completed or not
 * @returns {Object} action
 */
export const addTodo = (todo) => ({
  type: ADD_TODO,
  payload: { todo },
});

/**
 * @param {Object} updatedTodo - updated todo
 * @param {number} updatedTodo.id - id todo
 * @param {string} updatedTodo.text - todo title
 * @param {boolean} updatedTodo.isCompleted - toggle to check if value completed or not
 * @returns {Object} action
 */
export const updateTodo = (updatedTodo) => ({
  type: UPDATE_TODO,
  payload: { updatedTodo },
});

/**
 * @param {number} todoId - id todo
 * @returns {Object} action
 */
export const deleteTodo = (todoId) => ({
  type: DELETE_TODO,
  payload: { todoId },
});

/**
 * @param {number} todoId - id todo
 * @returns {Object} action
 */
export const completeTodo = (todoId) => ({
  type: COMPLETE_TODO,
  payload: { todoId },
});

export const filterCompletedTodo = () => ({
  type: FILTER_COMPLETED_TODO,
});

export const clearFilters = () => ({
  type: CLEAR_FILTERS,
});

export const searchByName = (searchQuery) => ({
  type: SEARCH_BY_NAME,
  payload: { searchQuery },
});

/**
 *
 * @param {Object} state - global state. First value: initial state
 * @param {Array} state.todos - todos array
 * @param {Object} action - action to change the state
 * @param {string} action.type - type of current action:
 *  ADD_TODO | UPDATE_TODO | DELETE_TODO | COMLETE_TODO
 * @param {*} action.payload - data for changing the state
 */
export const todoReducer = (state = initialState, action) => {
  console.log(action);
  console.log(state);

  switch (action.type) {
    case LOAD_TODO:
      return {
        ...state,
        todos: [...action.payload.todos],
        initialTodos: [...action.payload.todos],
      };
    case ADD_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload.todo],
        initialTodos: [...state.todos],
      };
    case UPDATE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo) =>
          todo.id === action.payload.updatedTodo.id
            ? action.payload.updatedTodo
            : todo
        ),
        initialTodos: [...state.todos],
      };
    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter(
          (todoItem) => todoItem.id !== action.payload.todoId
        ),
        initialTodos: [...state.todos],
      };
    case COMPLETE_TODO:
      return {
        ...state,
        todos: state.todos.map((todoItem) => {
          return todoItem.id === action.payload.todoId
            ? {
                ...todoItem,
                isCompleted: !todoItem.isCompleted,
              }
            : todoItem;
        }),
        initialTodos: [...state.todos],
      };
    case FILTER_COMPLETED_TODO:
      return {
        ...state,
        todos: state.todos.filter((todoItem) => !todoItem.isCompleted),
      };
    case CLEAR_FILTERS:
      return {
        ...state,
        todos: [...state.initialTodos],
      };
    case SEARCH_BY_NAME:
      return {
        ...state,
        todos: state.todos.filter((todoItem) =>
          todoItem.text.includes(action.payload.searchQuery)
        ),
      };
    default:
      return state;
  }
};
