import {
  FAILURE_COMMENTS,
  LOAD_COMMENTS,
  ADD_COMMENT,
} from '../actions/comments';

// початковий стейт
export const initialState = {
  comments: [],
};

export const commentsReducer = function (state = initialState, action) {
  switch (action.type) {
    case LOAD_COMMENTS:
      return {
        ...state,
        comments: [...action.payload.comments],
      };
    case FAILURE_COMMENTS:
      return {
        ...state,
        error: { ...action.payload.error },
      };
    case ADD_COMMENT:
      return {
        ...state,
        comments: [...state.comments, action.payload.comment],
      };
    default:
      return state;
  }
};
