import React, { useState } from 'react';

const tasksArr = [
    {
        id: 1,
        title: 'task1',
        isDone: false,
    }, {
        id: 2,
        title: 'task2',
        isDone: true,
    }, {
        id: 3,
        title: 'task3',
        isDone: false,
    },
]

export default function App() {
    console.log('App');
    let [reactCounter, setReactCounter] = useState(5); // завжди масив з двох значень, друге з яких завжди функція
    let [tasks, setTasks] = useState(tasksArr);
    const title = 'Hello World';
    // let counter = 5;
    console.log(tasks);

    function handleClick(event) {
        console.log(event);
    }

    function incrementCounter(event) {
        // console.log('incremented', counter);
        // counter++
        //reactCounter = 10;
        //setReactCounter(reactCounter);
        setReactCounter(reactCounter++); // ця функція асінхронна
    }

    // <React.Fragment></React.Fragment>
    return (
        <>
            <h1 id='header' className='style'>{title}</h1>
            <h3>Counter: {reactCounter}</h3>
            <button onClick={handleClick}>Click me</button>
            <button onClick={incrementCounter}>Increment counter</button>
            <ul>
                {
                    tasks.map((task) => 
                        <li style={{
                            textDecoration: task.isDone ? 'line-through' : 'auto'
                        }} key={task.id}>{task.title}</li>
                    )
                }
            </ul>
        </>
    );
}