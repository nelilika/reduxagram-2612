import { FAILURE_POSTS, LOAD_POSTS } from '../actions/posts';

// початковий стейт
export const initialState = {
  posts: [],
  selectedPost: {}, // або можна зробити null
  error: null,
  totalCount: 0,
  limit: 5,
};

// редьюсер
export const postsReducer = function (state = initialState, action) {
  switch (action.type) {
    case LOAD_POSTS:
      return {
        ...state,
        posts: [...action.payload.posts],
        totalCount: action.payload.totalCount,
      };
    case FAILURE_POSTS:
      return {
        ...state,
        error: { ...action.payload.error },
      };
    default:
      return state;
  }
};
