/* Задача 1
Напишіть програму, яка питає у користувача номер автобуса.
Якщо це номер 7, 255 или 115, тоді користувач може їхати.
Виведіть в консолі "You can go".
Якщо ні - виведіть в консолі "Please wait".
*/
{
  const busNumber = +prompt('Please enter the bus number', '7');

  // Варіант 1
  if (busNumber === 7 || busNumber === 225 || busNumber === 115) {
    console.log('You can go');
  } else {
    console.log('Please wait');
  }

  // Варіант 2
  switch (busNumber) {
    case 7:
    case 225:
    case 115:
      console.log('You can go');
      break;
    default:
      console.log('Please wait');
      break;
  }
}

/*Задача 2
Напишіть програму, яка отримує від користувача число і порівнює його з числом Pi. (Math.PI)
Порівняйте, чи є це число більше ніж число Pi.
Порівняйте, чи є це число менше ніж число Pi.
Порівняйте, чи це число дорівнює числу Pi.
Якщо введене значення не є числов, виведіть в консоль повідомлення про помилку.
Всі результати відобразіть в наступному форматі:
  You entered: <number> 
  Is greater then PI: true
  Is less then PI: false
  Is equal PI: false
*/

{
  const userNumber = +prompt('Please enter your number', '3.14');

  console.log(`
    You entered: ${userNumber}
    Is greater then PI: ${userNumber > Math.PI}
    Is less then PI: ${userNumber < Math.PI}
    Is equal PI: ${userNumber === Math.PI}
  `);
}

/* Задача 3
Напишіть програму, яка пропонує користувачу ввести пароль і перевіряє,
чи є ций пароль надійним за наступними умовами:
Пароль повинен бути не менше шести символів;
Пароль не повинен бути рівним строкам qwerty чи 123456;
Пароль повинен мати хоча б одну велику літеру.
Якщо всі умови виконані, виведіть в консоль повідомлення "Strong".

Якщо пароль має хоча б одну велику літеру але складається з п'яти символів,
виведіть в консоль повідомлення "Middle".
У всіх інших випадках, виведіть в консоль повідомлення "Weak".
*/

{
  const password = prompt('Enter your password');
  const isMiddlePassword =
    password.toLowerCase() !== 'qwerty' &&
    password !== '123456' &&
    password !== password.toLowerCase() &&
    password.length >= 5;
  let result = 'Weak';

  if (isMiddlePassword) {
    result = password.length >= 6 ? 'Strong' : 'Middle';
  }

  console.log(result);
}
