export const LOAD_COMMENTS = '[COMMENTS] Load Comments';

export const loadComments = (comments) => ({
  type: LOAD_COMMENTS,
  payload: { comments },
});
