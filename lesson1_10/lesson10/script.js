// rest operator (...)
// spread operator (...)

const array = [1, 2, 3];
const newArray = [...array];

const obj = { a: 1 };
const newObject = { ...obj };

// function someFunc(...args) {
//   // arguments - псевдо масив  []
//   // let args = [...arguments]; - є справжнім масивом
//   console.log(args);
// }

function someFunc(param1, ...args) {
  // arguments - псевдо масив  []
  // let args = [...arguments]; - є справжнім масивом
  console.log(args);
  console.log(param1);
}

someFunc(1, 2, 3);
someFunc(3, 4, '5');
someFunc({}, 'qwerty', null, []);

console.log('Max', Math.max(...array));
console.log('Min', Math.min(...array));
