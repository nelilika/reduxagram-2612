import { Component } from 'react';
import './App.css';
import SearchPhoto from './components/SearchPhoto';
import ImageList from './components/ImageList';
import { getImagesByName } from './api/unsplashApi';

/* Функціональний компонент */
// function App() {
//   return <h1>Class Components</h1>;
// }

/* Класовий компонент */
class App extends Component {
  constructor() {
    super();
    this.state = {
      images: [],
    };
  }

  /**
   * useEffect(() => getImagesByName().then(res => console.log(res)), []);
   */
  // componentDidMount() {
  //   console.log('hello componentDidMount');
  //   getImagesByName('cats').then(({ data: { results: images } }) => {
  //     this.setState({ images });
  //   });
  // }

  onSearchSubmit = async (query) => {
    const {
      data: { results: images },
    } = await getImagesByName(query);
    this.setState({ images });
  };

  render() {
    console.log('hello render');
    return (
      <>
        <h1>Class Components</h1>
        <SearchPhoto onSearchSubmit={this.onSearchSubmit} />
        <ImageList images={this.state.images} />
      </>
    );
  }
}

export default App;
