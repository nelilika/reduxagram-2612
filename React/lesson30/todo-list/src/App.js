import React from 'react';
import TodoWrapper from './components/TodoWrapper/TodoWrapper';
import TodoProvider from './context/Context';

function App() {
  return (
    <TodoProvider>
      <TodoWrapper />
    </TodoProvider>
  );
}

export default App;
