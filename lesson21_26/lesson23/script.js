// Method GET

async function addNewPost() {
  return fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify({
      text: 'Some text in post',
    }),
  });
}

// async / await

async function getName() {
  // return Promise.resolve('Aurora');
  return 'Aurora'; // resolve data
}

getName().then((res) => {
  console.log('Hello, dear ' + res);
});

// await === then

// try / catch

/*try {

} catch (e) { === catch
  
}*/

async function start() {
  try {
    const name = await getName();
    console.log('Hello, dear ' + name);

    const rawData = await addNewPost();
    const posts = await rawData.json();
    console.log(posts);
  } catch (e) {
    console.error(e);
  }

  console.log('Bye');
}

start();

console.log('Hello');

// const p1 = Promise.resolve(1)
//   .then((res) => console.log(2)) // res === 1
//   .then((res) => console.log(3)) // res === undefined
//   .then((res) => console.log(4));

// const p2 = Promise.resolve(5)
//   .then((res) => console.log(6)) // res === 5
//   .then((res) => console.log(7)) // res === undefined
//   .then((res) => console.log(8));

// Promise.all([p1, p2]); // 2 6 3 7 4 8

// const p3 = Promise.resolve(1)
//   .then((res) => console.log(2))
//   .then((res) => {
//     setTimeout(() => {
//       console.log(3);
//     }, 1000);
//     return 3;
//   })
//   .then((res) => console.log('test'))
//   .then((res) => console.log(4));
// 2 4 undefined 3

// p3.then((res) => console.log(res)); // undefined

// Приклад того, як через метод GET можна відправляти дані (не через body, а через queryParams)
fetch(
  'https://jsonplaceholder.typicode.com/posts?username=nelilika&password=qwerty'
);

Promise.resolve(1)
  .then((res) => console.log(2))
  .then((res) => Promise.reject(3))
  .then((res) => console.log(4))
  .then((res) => console.log(5))
  .catch((res) => console.log(6))
  .then((res) => console.log(7));
// 2 6 7

// Tips: Класс Співробітники
class Worker {}

class Boos extends Worker {}

const empoyees = [
  {
    name: '',
    position: 'boss',
    salary: 1243,
  },
  {
    name: '',
    position: 'boss',
    salary: 1243,
  },
  {
    name: '',
    position: 'boss',
    salary: 1243,
  },
];

for (let empoyee of empoyees) {
  if (position === 'boss') {
    new Boss(empoyee.name, empoyee.salary);
  }
  new Worker(empoyee.name, empoyee.salary);
}
