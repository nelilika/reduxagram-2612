import todos from '../data/todos.json';

export function getTodo() {
  return Promise.resolve(todos);
}
