import React, { useEffect, useState, useMemo } from 'react';
import PostGrid from '../components/Posts/PostGrid';
import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import { fetchPosts } from '../store/reducers/postsSlice';
import Pagination from '@mui/material/Pagination';
import { getPages } from '../utils/getPages';
import { useSelector } from 'react-redux';
import PostPagination from '../components/UI/Pagination';
import CircularProgress from '@mui/joy/CircularProgress';
import Box from '@mui/joy/Box';

export default function Posts() {
  const dispatch = useDispatch();
  const { limit, totalCount, loading, loaded } = useSelector(
    (state) => state.postsReducer
  );
  const [page, setPage] = useState(1);

  useEffect(() => {
    dispatch(fetchPosts({ limit, page })); // це початковий для асінхронного запиту діпатч екшна
    // який викликає рекверст на сервер
  }, [dispatch, page]);

  const pages = useMemo(() => getPages(limit, totalCount), [limit, totalCount]);

  if (!loaded && loading) {
    return (
      <Box
        sx={{ display: 'flex', gap: 2, alignItems: 'center', flexWrap: 'wrap' }}
      >
        <CircularProgress variant="solid" />
      </Box>
    );
  }

  return (
    <>
      <Typography
        variant="h1"
        component="h2"
        sx={{
          textAlign: 'center',
          fontFamily: 'Festive',
          color: 'lightBlue',
          m: '15px 0',
        }}
      >
        Reduxagram
      </Typography>
      <PostPagination
        count={pages}
        currentPage={page}
        handleChange={(_, value) => setPage(value)}
      />
      <PostGrid />
    </>
  );
}
