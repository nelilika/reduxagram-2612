/* Завдання №145*/
/* Написати програму, яка питає у користувача його зріст і виводить його в консоль в форматі:
    "Ваш вік: 135кг!"
*/
{
    // const growth = prompt('Enter your tall'); // 124
    // console.log(growth);  // '124'
}

// 3) value: 123456; type: number

//console.log(typeof number);
const growth = 1243;

// Нова тема
// Примітивні типи даних
/*number
string
boolean
undefined
null

symbol
bigInt

// Об'єкт
object*/12


// Number
// всі числа, числовий тип даних

const integer = 2; // 34, 56, -67
const double = 3.4; // -5.7, 87.34635, 3.1415926
const zero = 0; // +0, -0
const infinity = Infinity; // -Infinity

// ділення на 0 дає нескінченність - infinity

const nan = NaN; // not a nuber

const bigNumber = 1e3; // 1000
const smallNumber = 1e-3;  // 0.001

const binary = 0b101; // 5

// !!!
// 0.1 + 0.2 не дорівнює 0.3

// Числові методи
// number.toFixed(2) -> повертає строкове значення
let sum = 0.1 + 0.2; 

// isNaN();
// isFinite();
// number.toString(); ->  повертає строкове значення
// parseInt(str);
// parseFloat(str);


// String
// всі рядки, строковий тип даних

const one = 'Hello World'; // рекомендовано 
const two = "Hello World";
const three = `Hello World`;

const four = '\'Ваш вік: 135кг!\''; // екранування символів

const string = 'Four \\ five';


const multipleString = "\tHello\nI\'m Teacher"; 
console.log(multipleString);

const name = 'Teacher';
const string2 = `Hello
I'm ${name}`;
console.log(string2);

// Методи рядків
// string.toUpperCase();
// string.toLowerCase();
// string.length

// Boolean
// Логічний тип

const trueB = true;
const falseB = false;

// Математичні оператори

// + - * /
// > < >= <= -> працюють тільки з логічним типом даних
// ++, --

// Унарний опертор
// Постфіксний Інкремент/Дікремент
// number++; - збільшує (зменшує) його на одиницю

let number = 56;
// number = 56 + 1;

// Префіксний Інкремент/Дікремент
// ++number;

// Пріорітети операторів

number++ + ++number // 114
// (56 + 1 -> 56) + (57 + 1 -> 58) -> 56 + 58 = 114


// Логічні оператори
// Логічне АБО (||)
// Логічне І (&&)
// Логічне НЄ (!)

// undefined -> false
// null -> false

5 || false || 'summer' || undefined // -> 5
// true || false || true || false 

0 || false || 'summer' || undefined // -> 'summer'
// false || false || true || false

0 || false || '' || undefined // -> undefined
// false || false || false || false 

5 && false && 'summer' && undefined // -> false
// true && false && true && false 

5 && true && 'summer' && undefined // -> undefined
// true && true && true && false 

!5 && true && 'summer' && 34 // -> 34
// true && true && true && false 

45 || undefined && null || '7'

// && має вищий пріорітет операцій за ||