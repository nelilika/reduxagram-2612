export default class Student {
  constructor(name, mark) {
    this.name = name;
    this.mark = mark;
  }

  getInfo() {
    return `${this.name} has ${this.mark}`;
  }
}
