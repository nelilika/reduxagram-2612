import React from 'react';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import PropTypes from 'prop-types';

export default function PostPagination({ count, currentPage, handleChange }) {
  return (
    <Stack spacing={2} sx={{ alignItems: 'center' }}>
      <Pagination
        count={count}
        page={currentPage}
        onChange={handleChange}
        color="primary"
      />
    </Stack>
  );
}

PostPagination.propTypes = {
  count: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
};
