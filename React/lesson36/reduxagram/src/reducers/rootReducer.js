import { combineReducers } from 'redux';
import { postsReducer } from './posts';
import { commentsReducer } from './comments';
import { usersReducer } from './users';

export const rootReducer = combineReducers({
  postsReducer,
  commentsReducer,
  usersReducer,
});
