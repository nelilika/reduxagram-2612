import React from 'react';
import TodoWrapper from './components/TodoWrapper';
import TodoContext from './context/Context';

function App() {
  return (
    <TodoContext>
      <TodoWrapper />
    </TodoContext>
  );
}

export default App;
