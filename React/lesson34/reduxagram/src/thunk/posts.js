import { getPosts } from '../api';
import { loadPosts, failurePosts } from '../actions/posts';

// export const fetchPosts = ({ limit, page }) => {
//   console.log(limit, page);
//   return function (dispatch) {
//     getPosts({ limit, page }).then(
//       // це кінцевий діспатч екшна, який вже має дані з сервера і буде оновлювати глобальний стейт
//       (posts) => dispatch(loadPosts(posts))
//     );
//   };
// };

export const fetchPosts = (params) => {
  return async (dispatch) => {
    try {
      const { data: posts, totalCount } = await getPosts(params);
      dispatch(loadPosts(posts, totalCount));
    } catch (e) {
      dispatch(failurePosts(e));
    }
  };
};
