let blocks = document.querySelectorAll('.content');
let slaiderLine = document.querySelector('.slaider__line');
let next = document.querySelector('.slaider__next');
let prev = document.querySelector('.slaider__prev');

let ofset = 0;
let itemShow = 2;
let itemScroll = 1;
let itemWidth = 350;
let scrollWidth = itemWidth * itemScroll;
let slaiderWidth = -itemWidth * (blocks.length - itemShow);
setInterval(slideNext, 3000);

next.addEventListener('click', slideNext);

prev.addEventListener('click', slideLeft);

blocks.forEach((block, index) => {
  block.textContent = '';
  const img = document.createElement('img');
  img.setAttribute('src', `./images/dog-${index + 1}.jpeg`);
  img.setAttribute('alt', `dog-${index + 1}`);
  img.classList.add('slaider__image');
  block.append(img);
});

function slideNext() {
  if (ofset === slaiderWidth) {
    ofset = 0;
  } else {
    ofset -= scrollWidth;
  }

  slaiderLine.style.transform = `translate3d(${ofset}px, 0, 0)`;
}

function slideLeft() {
  if (ofset === 0) {
    ofset = slaiderWidth;
  } else {
    ofset += scrollWidth;
  }

  slaiderLine.style.transform = `translate3d(${ofset}px, 0, 0)`;
}

document.body.addEventListener('keydown', (event) => {
  console.log(event);
  if (event.key === 'ArrowRight') {
    slideNext();
  }
  if (event.key === 'ArrowLeft') {
    slideLeft();
  }
});
