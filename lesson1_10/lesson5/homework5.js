/* Завдання 1.
Напишіть програму, яка питає у користувача його імʼя і виводить в консолі 
текстове привітання.
Happy birthday to you
Happy birthday to you
Happy birthday, dear <name>
Happy birthday to you
 */

{
  const firstName = prompt('Enter your name:', 'Anna');

  for (let i = 0; i < 3; i++) {
    if (i === 2) {
      console.log(`Happy birthday, dear ${firstName}`);
    }
    console.log('Happy birthday to you');
  }
}

/* Завдання 2.
  Сформуйте строку '.#.#.#.#.#.#.#' за допомогою циклу for,
де необхідну кількість повторів символів '.#' задає користувач через 
команду prompt().
 */

{
  const number = parseInt(prompt('Enter the number', '1'));
  let str = '';

  for (let i = 1; i <= number; i++) {
    str += '.#';
  }
  console.log(str);
}

/* Завдання 3.
  Напишіть програму, яка питає у користувача число і сумує всі непарні числа до цього числа.
Якщо користувач ввів не число або відʼємне число чи 0, визивати команду prompt() з текстом "Invalid.
You should enter a number" до тих пір поки вірний формат даних не буде введений користувачем.
Результат відобразіть в консолі.
 */

{
  let number = +prompt('Enter any positive integer number > 0');
  let sum = 0;

  while (number <= 0 || !isFinite(number) || !Number.isInteger(number)) {
    number = prompt('Invalid. You should enter a number');
  }

  for (let i = 1; i < Number(number); i = i + 2) {
    sum += i;
  }

  console.log(sum);
}

/* Завдання 4.
Напишіть нескінченний цикл, який закінчується командою break, коли Math.random() > 0.7.
Виведіть в консоль число, на якому переривається цикл та відобразіть в консолі кількість ітерацій циклу.
Loop was finished because of: <number>
Number of attempts: <number>
*/

{
  let value = 0;

  for (let i = 0; true; i++) {
    value = Math.random();
    if (value > 0.7) {
      console.log(`Loop was finished because of: ${value}`);
      console.log(`Number of attempts: ${i + 1}`);
      break;
    }
  }
}

/* Завдання 5.
  Напишіть цикл від 1 до 50, в якому будуть виводитися числа по черзі від 1 до 50, при цьому:
Якщо число ділиться на 3 без залишку, то виведіть не це число, а слово 'Fizz';
Якщо число ділиться на 5 без залишку, то виведіть не це число, а слово 'Buzz';
Якщо число ділиться і на 3 і на 5 одночасно, то виведіть не це число, а слово 'FizzBuzz';
 */

{
  for (let i = 0; i < 50; i++) {
    console.log((++i % 3 ? '' : 'Fizz') + (i % 5 ? '' : 'Buzz') || i);
  }
}

{
  for (let i = 1; i <= 50; i++) {
    let res = '';

    if (!(i % 3)) {
      res += 'Fizz';
    }

    if (!(i % 5)) {
      res += 'Buzz';
    }

    console.log(res || i);
  }
}

/* Завдання 6.
Напишіть програму, яка знайде всі роки, коли 1 січня випадає на неділю у період між 2015 та 2050 роками включно 
(зверніть увагу, що 1 січня в лапках).
"1st of January" is being a Sunday in <year>
 */

for (let i = 2015; i <= 2050; i++) {
  let date = new Date(`${i}-1-1`);
  if (!date.getDay()) {
    console.log(`"1st of January" is being a Sunday in ${i}`);
  }
}
