import React, { useState } from 'react';
import './App.scss';
import Markup from './components/Markup';
import ProjectModal from './components/UI/Modal/ProjectModal';
import AddProject from './components/AddProject';

function App() {
  const [visible, setVisible] = useState(false);

  return (
    <>
      <ProjectModal visible={visible} setVisible={setVisible}>
        <AddProject />
      </ProjectModal>
      <Markup />
    </>
  );
}

export default App;
