/* Завдання 1 */
// Перевірити, що файл має розширення .pdf або .xml
{
  const regExp = /.\.(pdf|xml)$/; // your regExp

  const validCases = ['homework.pdf', 'practice.xml'];
  const invalidCases = [
    'asdfg',
    'image.jpg',
    'homework.pdfewe',
    'qpdf',
    '.pdf',
  ];

  console.log('Task 1:', testRegExp(regExp, validCases, invalidCases));
}

/* Завдання 2 */
// Перевірити валідність номеру телефона оператора Lifecel
// Номер повинен починатися з +380
// Далі мати дві цифри 63 або 93
// Потім 7 довільних цифр

{
  const regExp = /^\+380[69]3\d{7}$/; // your regExp

  const validCases = ['+380631234567', '+380939876543'];
  const invalidCases = [
    'asdfg',
    '+380501234567',
    '+480631234567',
    'ewr+380631234567ew',
    '+3806312',
  ];

  console.log('Task 2:', testRegExp(regExp, validCases, invalidCases));
}

/* Завдання 3 */
// Перевірити формат відповіді наступного рядка
// Моє день народження <number> березня
// Число повинно бути від 1 до 31

{
  const regExp = /\b([1-9]|[12]\d|3[01])\b/; // your regExp

  const validCases = [
    'Моє день народження 1 березня',
    'Моє день народження 31 березня',
    'Моє день народження 20 березня',
    'Моє день народження 15 березня',
  ];
  const invalidCases = [
    'Моє день народження 0 березня',
    'Моє день народження 32 березня',
    'Моє день народження 552 березня',
  ];

  console.log('Task 3:', testRegExp(regExp, validCases, invalidCases));
}

/* Завдання 4 */
// Порахувати кількість слів в тексті
{
  const regExp = /[a-zA-Z]+/gi; // your regExp

  const validCases = [
    {
      text: 'qwerty qwerty',
      value: 2,
    },
    {
      text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      value: 26,
    },
    {
      text: 'I love JavaScript',
      value: 3,
    },
  ];

  console.log('Task 4:', testRegExp(regExp, validCases));
}

/* Завдання 5 */
// Валідація нецензурних слів
// Будь-яке слово, яке починається на hu замінити 'hu' на **
// окрім слова hugo.
{
  const regExp = /hu[^g]/g;
  const testString = 'hugo hu qwerty huwerty hug';

  const result = testString.replace(regExp, '**');
  console.log(result);
}

function testRegExp(regExp, validCases, invalidCases) {
  if (typeof validCases[0] === 'string') {
    const checkValidCases = validCases.every((testCase) =>
      regExp.test(testCase)
    );
    const checkInvalidCases = invalidCases.every(
      (testCase) => !regExp.test(testCase)
    );
    return checkValidCases && checkInvalidCases;
  }
  return validCases.every(
    (testCase) => testCase.text.match(regExp).length === testCase.value
  );
}
