export const getPages = (limit, totalCount) => Math.ceil(totalCount / limit);
