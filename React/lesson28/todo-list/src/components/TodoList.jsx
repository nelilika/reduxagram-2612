import React from 'react';
import TodoItem from './TodoItem';

export default function TodoList({
  todos,
  deleteTodo,
  toggleCompletedTodo,
  updateTodo,
}) {
  return (
    <div className="todo-list">
      {todos.map((todoItem) => (
        <TodoItem
          todo={todoItem}
          key={todoItem.id}
          deleteTodo={deleteTodo}
          toggleCompletedTodo={toggleCompletedTodo}
          updateTodo={updateTodo}
        />
      ))}
    </div>
  );
}
