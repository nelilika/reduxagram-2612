import React from 'react';
import './AddProject.scss';

function AddProject() {
  return (
    <>
      <h3 className="app-name">Add new project</h3>
      <form className="new-project">
        <label htmlFor="title">Project title</label>
        <div className="search-wrapper new-project">
          <input
            id="title"
            className="search-input"
            type="text"
            placeholder="Enter title"
          />
        </div>

        <label htmlFor="startDate">Start date</label>
        <div className="search-wrapper new-project">
          <input id="startDate" className="search-input" type="date" />
        </div>

        <label htmlFor="deadline">Deadline</label>
        <div className="search-wrapper new-project">
          <input id="deadline" className="search-input" type="date" />
        </div>

        <button className="create-project">Create Project</button>
      </form>
    </>
  );
}

export default AddProject;
