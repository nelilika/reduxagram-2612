export const LOAD_COMMENTS = '[COMMENTS] Load Comments';
export const FAILURE_COMMENTS = '[COMMENTS] Failure Comments';

export const loadComments = (comments) => ({
  type: LOAD_COMMENTS,
  payload: { comments },
});

export const failureComments = (error) => ({
  type: FAILURE_COMMENTS,
  payload: { error },
});
