// Promises

const getNamePromise = new Promise(function () {});
const getName = function () {
  console.log('Hello world');
};

// Приклад роботи з асінхронністю через функції колбек
function getServerResponse(callback) {
  console.log('stared getting data from server');
  // 2) через 4 секунди функція-колбек setTimeout
  setTimeout(() => {
    const student = {
      id: 1234,
      name: 'Witek',
      age: 15,
    };
    // 3 callback()
    callback(student);
  }, 4000);
}

function callbackFn(response) {
  console.log('got data from server');
  console.log(response);
}

// 1) getServerResponse()
// getServerResponse(callbackFn);

console.warn('Create our very first promise');
const promise = new Promise(function (resolve, reject) {
  setTimeout(() => {
    const student = {
      id: 1234,
      name: 'Witek',
      age: 15,
    };
    resolve(student);
  }, 4000);
});

console.log(promise);

promise
  .then((res) => {
    // Promise.prototype.then - перехоплює тільки успішні дані
    console.log('Data from promise:', res);
  })
  .catch((rej) => {
    // Promise.prototype.catch - перехоплює тільки НЕ успішні дані
    console.error('Error from promise:', rej);
  })
  .finally(() => {
    // Promise.prototype.finally
    console.log('Finally called');
  });

console.warn('Finished created our very first promise');

// Статичні методи
// 1)  Promise.resolve()
const resolvedPromise = Promise.resolve({
  // завжди успішний проміс
  msg: 'Hello from static resolved promise',
});

console.log(resolvedPromise);

resolvedPromise.then((res) => {
  console.log(res);
});

// 2)  Promise.reject()
const rejectedPromise = Promise.reject({
  // завжди НЕуспішний проміс
  err: 'Unfortunately, not success',
});

console.error(rejectedPromise);

rejectedPromise.catch((err) => {
  console.error(err);
});

// 3) Promise.all([])
// const allPromises = Promise.all([
//   generatePromise('First', 3000, false),
//   generatePromise('Second', 1000, false),
//   generatePromise('Third', 1500),
// ]);
// .then((res) => console.log(res))
// .catch((res) => console.error(res));

// console.log(allPromises);

// 4) Promise.race([])
// const racePromises = Promise.race([
//   generatePromise('First', 3000),
//   generatePromise('Second', 1000),
//   generatePromise('Third', 500),
// ])
//   .then((res) => console.log(res))
//   .catch((res) => console.error(res));

// console.log(racePromises);

function generatePromise(message, time, isSuccess = true) {
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      console.warn(message);
      if (isSuccess) {
        resolve(message);
      } else {
        reject(message);
      }
    }, time);
    return; // він буде ігноруватися
  });
}

const p1 = Promise.resolve(1)
  .then((res) => console.log(res))
  .then(() => {
    return new Promise((res, rej) => {
      rej('Custom error');
    });
  })
  .catch((err) => console.error(err));

const p2 = Promise.all([Promise.resolve('resolved')])
  .then((res) => console.log(res)) // ['resolved']
  .then((res) => Promise.reject('Erorr'))
  .then((res) => console.log(3)) // ігноруватися
  .then((res) => console.log(4)) // ігноруватися
  .catch((err) => console.error(err)) // 'Erorr'
  .then(() => console.log('Hello after catch'));
