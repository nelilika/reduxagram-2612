import axios from 'axios';
import { INSTAGRAM_URL } from '../config';

const instagramAxios = axios.create({
  baseURL: INSTAGRAM_URL,
  timeout: 1000,
});

export const getPosts = async function ({ limit: _limit, page: _page }) {
  const { data } = await instagramAxios.get('/posts', {
    params: {
      _limit,
      _page,
    },
  });
  return data;
};

export const getPost = function (id) {
  return instagramAxios.get(`/posts/${id}`);
};

export const getComments = function (id) {
  return instagramAxios.get(`/posts/${id}/comments`);
};
