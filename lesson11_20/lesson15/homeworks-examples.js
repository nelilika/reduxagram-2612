// ДЗ 3
// 3) Задача про підʼізди і етажі
const roomsOnFloor = 3;
const floors = 9;
const roomNumber = prompt();

{
  const userFloor = Math.ceil((roomNumber / roomsOnFloor) % floors) || 9;
  const userEntrance = Math.ceil(roomNumber / (roomsOnFloor * floors));
}

// ДЗ 4
// 1) Задача про номер автобусу 7, 115 або 255 (||)
// if (busNumber === 7 || busNumber === 115)
{
  switch (busNumber) {
    case '7':
    case '115':
    case '255':
      console.log('You can go');
      break;
    default:
      console.log('Please wait');
      break;
  }
}

// 2) Задача про порівняння PI
/*
You entered: <number> 
Is greater then PI: true
Is less then PI: false
Is equal PI: false 
*/
{
  console.log(`Is greater then PI: ${number > numberPi}`);
  console.log(`Is less then PI: ${number < numberPi}`);
  console.log(`Is equal PI:  ${number === numberPi}`);
}

// 3) Задача про паролі

// const password = prompt('Enter password:');
// if (
//   password.length === 5 &&
//   password.toLowerCase() !== password &&
//   password !== 'qwerty' &&
//   password !== '123456'
// ) {
//   console.log('Middle');
// } else if (
//   password.length >= 6 &&
//   password.toLowerCase() !== password &&
//   password !== 'qwerty' &&
//   password !== '123456'
// ) {
//   console.log('Strong');
// } else {
//   console.log('Weak');
// }

if (
  password.toLowerCase() !== password &&
  password !== 'qwerty' &&
  password !== '123456' &&
  password.length >= 5
) {
  if (password.length === 5) {
    console.log('Middle');
  } else {
    console.log('Strong');
  }
} else {
  console.log('Weak');
}

{
  const password = prompt('Enter your password');
  const isMiddlePassword =
    password !== 'qwerty' &&
    password !== '123456' &&
    password !== password.toLowerCase() &&
    password.length >= 5;
  let result = 'Weak';

  if (isMiddlePassword) {
    result = password.length >= 6 ? 'Strong' : 'Middle';
  }

  console.log(result);
}

// ДЗ 5
// 1) Задача про День Народження
{
  const firstName = prompt('What is your name?', 'Visitor');
  for (let i = 0; i < 3; i++) {
    if (i === 2) {
      console.log(`Happy birthday, dear ${firstName}`);
    }
    console.log('Happy birthday to you');
  }
}

// 3) Задача про суму непарних чисел
{
  let number = +prompt();

  for (let i = 1; i < +number; i = i + 2) {
    sum += i;
  }
}

// 5) FizzBuzz
{
  for (let i = 0; i < 50; i++) {
    console.log((++i % 3 ? '' : 'fizz') + (i % 5 ? '' : 'buzz') || i);
  }
}

{
  for (let i = 1; i <= 50; i++) {
    let res = '';

    if (!(i % 3)) {
      res += 'Fizz';
    }

    if (!(i % 5)) {
      res += 'Buzz';
    }

    console.log(res || i);
  }
}

// ДЗ 7
// 3 d) Задача про двох жіночок)
{
  const employees = [
    {
      id: 1,
      name: 'Karan',
      surname: 'Duffy',
      salary: 1010,
      workExperience: 10,
      isPrivileges: false,
      gender: 'male',
    },
    {
      id: 2,
      name: 'Brax',
      surname: 'Dalton',
      salary: 1200,
      workExperience: 12,
      isPrivileges: false,
      gender: 'male',
    },
    {
      id: 3,
      name: 'Jody',
      surname: 'Lam',
      salary: 480,
      workExperience: 3,
      isPrivileges: true,
      gender: 'female',
    },
    {
      id: 4,
      name: 'Ceara',
      surname: 'Zuniga',
      salary: 2430,
      workExperience: 20,
      isPrivileges: false,
      gender: 'female',
    },
    {
      id: 5,
      name: 'Walid',
      surname: 'Hulme',
      salary: 3150,
      workExperience: 30,
      isPrivileges: true,
      gender: 'female',
    },
    {
      id: 6,
      name: 'Hilary',
      surname: 'Oliver',
      salary: 1730,
      workExperience: 15,
      isPrivileges: false,
      gender: 'male',
    },
    {
      id: 7,
      name: 'Tye',
      surname: 'Herring',
      salary: 5730,
      workExperience: 45,
      isPrivileges: true,
      gender: 'male',
    },
    {
      id: 8,
      name: 'Marc',
      surname: 'Ellison',
      salary: 4190,
      workExperience: 39,
      isPrivileges: false,
      gender: 'male',
    },
    {
      id: 9,
      name: 'Deon',
      surname: 'Lindsay',
      salary: 790,
      workExperience: 7,
      isPrivileges: false,
      gender: 'male',
    },
    {
      id: 10,
      name: 'Marlene',
      surname: 'Rogers',
      salary: 5260,
      workExperience: 49,
      isPrivileges: true,
      gender: 'female',
    },
    {
      id: 11,
      name: 'Rodrigo',
      surname: 'Kouma',
      salary: 300,
      workExperience: 1,
      isPrivileges: false,
      gender: 'male',
    },
  ];
  const femaleEmployees = {};

  for (let i = 0; i < employees.length; i++) {
    if (employees[i].gender === 'female') {
      femaleEmployees[
        employees[i].workExperience
      ] = `${employees[i].name} ${employees[i].surname}`;
    }
  }

  console.log(femaleEmployees);

  const [firstWoman, secondWoman] = Object.values(femaleEmployees);
  console.log(firstWoman);
  console.log(secondWoman);
}
