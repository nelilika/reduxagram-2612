import { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro';

/**
 * Щоб створити керований інпут в функціональному компоненті
 * const [searchInput, setSeatchInput] = useState('');
 *
 * <input value={value} onChange={(e) => setValue(e.target.value)}/>
 */

/* Класовий компонент */
class SearchPhoto extends Component {
  constructor() {
    // this
    super(); // отримує this від батьківського класу
    console.log(this);
    this.state = {
      searchInput: '',
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  // функція яку б ми писали для функціонального компоненту
  // function onFormSubmit(e) {
  //   e.preventDefault();
  // }

  // тут ми загубили контекст this бо маємо асінхронний event
  onFormSubmit(event) {
    event.preventDefault();
    this.props.onSearchSubmit(this.state.searchInput);
  }

  // стрілочна функція
  // onFormSubmit = (event) => {
  //   event.preventDefault();

  //   console.log(this);
  // };

  render() {
    return (
      <div className="wrap">
        <form className="search" onSubmit={this.onFormSubmit}>
          <input
            type="text"
            className="search-term"
            placeholder="What images are you looking for?"
            value={this.state.searchInput}
            onChange={(event) =>
              this.setState({
                searchInput: event.target.value,
              })
            }
          />
          <button type="submit" className="search-button">
            <FontAwesomeIcon icon={icon({ name: 'search' })} />
          </button>
        </form>
      </div>
    );
  }
}

export default SearchPhoto;
