console.log(window);

// console.log()
console.log(window.console);

console.group('Home work 16');
console.group('Task 1');
console.log(45);
console.groupEnd('Task 1');
console.group('Task 2');
console.log(45);
console.groupEnd('Task 2');
console.groupEnd('Home work 16');

// local storage
console.log(window.localStorage);
localStorage.setItem('someKey', 'someValue');
localStorage.removeItem('keyToRemove');
// localStorage.clear();
localStorage.setItem('someKey', 'someValue');
console.log(localStorage.getItem('someKey'));
localStorage.setItem('someKey', 'someNewValue');

console.log(employees);
localStorage.setItem('employees', employees); // [object Object], [object Object]
// localStorage працює виключно з рядками

localStorage.setItem('employees', JSON.stringify(employees)); // [object Object], [object Object]
console.log(typeof localStorage.getItem('employees')); // string

const newEmployees = JSON.parse(localStorage.getItem('employees'));

document.querySelector('button').addEventListener('click', () => {
  // window.open('https://lms.ithillel.ua/'); - відкриття нової вкладки по посиланню
  window.location.reload(); // оновить сторінку
});

// Geolocation

function success(pos) {
  const crd = pos.coords;

  console.log('Your current position is:');
  console.log(`Latitude : ${crd.latitude}`);
  console.log(`Longitude: ${crd.longitude}`);
  console.log(`More or less ${crd.accuracy} meters.`);
}

function error(err) {
  console.warn(`ERROR(${err.code}): ${err.message}`);
}

navigator.geolocation.getCurrentPosition(success, error);
