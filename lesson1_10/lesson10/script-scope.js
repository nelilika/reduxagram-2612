// Lexical scope
// Лексическая область видимости
let value = 15;
{
  let value = 10;

  function func() {
    console.log('Var1', value); // 10 - виклик
  }
  func();
}

{
  let value = 10;

  function func() {
    let value = 5; // ініціалізація
    console.log('Var2', value); // 5 - виклик
  }
  func();
  console.log('Var2', value); // 10
}

{
  let value = 10;

  function func(value) {
    console.log('Var3', value);
  }

  func(value);
  value = 5;
  func(value);
  console.log('Var3', value);
}

// Лексична область видимості

function calculator() {
  // local scope calculator
  let a = 4;
  let b = 5;

  function sum(a) {
    // local scope sum
    console.log(a + b); // 19
  }

  sum(10);
  b = 15;
}
calculator();
// Замикання
// function counter(number) {
//   let sum = 0;
//   sum += number;
//   console.log(sum);
//   return sum;
// }

// const sum = counter(3);
// console.log(sum);
// counter(4);
// counter(7);

function counter() {
  let x = 10;
  console.log('x', x);
  let sum = 0;

  return function (number) {
    sum += number;
    console.log(sum);
    return sum;
  };
}
// var2
function counter() {
  let x = 10;
  console.log('x', x);
  let sum = 0;

  function calculateSum(number) {
    sum += number;
    console.log(sum);
    return sum;
  }

  return calculateSum;
}

const calculateSum = counter(); // sum + number

calculateSum(3); // 3
calculateSum(4); // 7
calculateSum(7); // 14

counter();

calculateSum(10); // 24

//
for (let i = 0; i < 3; i++) {
  // i = 0
  setTimeout(function () {
    console.log('Timeout', i);
  }, 1000);
}
// console.log(i); // 3;

// не пишіть так пліііііз
function some() {
  globalValue = 'globalValue';
  var varValue = 'varValue';
}
some();
console.log(globalValue);
console.log(varValue);
