import { Component } from 'react';
import ImageItem from './ImageItem';

class ImageList extends Component {
  render() {
    console.log(this.props.images);
    if (!this.props.images.length) {
      return <h4>No results</h4>;
    }

    return (
      <div className="image-list">
        {this.props.images.map((image) => (
          <ImageItem key={image.id} image={image} />
        ))}
      </div>
    );
  }
}

export default ImageList;
