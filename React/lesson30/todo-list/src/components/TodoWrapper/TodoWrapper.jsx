import { useState, useEffect, useContext } from 'react';
import '../../App.scss';
import './TodoWrapper.scss';

import AddTodo from '../AddTodo/AddTodo';
import TodoList from '../TodoList';
import { getTodo } from '../../server/api';
import { TodoContextComponent } from '../../context/Context';
import {
  loadTodo,
  filterCompletedTodo,
  clearFilters,
} from '../../reducer/todoReducer';
import { FiCamera, FiCameraOff } from 'react-icons/fi';

function TodoWrapper() {
  const [, dispatch] = useContext(TodoContextComponent);
  const [filteredTodos, setFilteredTodos] = useState(false);

  useEffect(() => {
    getTodo().then((todos) => dispatch(loadTodo(todos)));
  }, []);

  useEffect(() => {
    if (filteredTodos) {
      dispatch(filterCompletedTodo());
    } else {
      dispatch(clearFilters());
    }
  }, [filteredTodos]);

  function toggleFilterState() {
    setFilteredTodos(!filteredTodos);
  }

  return (
    <div className="todo-app">
      <div className="filter-todo">
        {filteredTodos ? (
          <FiCameraOff onClick={toggleFilterState} />
        ) : (
          <FiCamera onClick={toggleFilterState} />
        )}
      </div>
      <h1>What's plans for today, bro?</h1>
      <AddTodo />
      <TodoList />
    </div>
  );
}

export default TodoWrapper;
