import React, { useState } from 'react';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

export default function PostPagination({ count, currentPage, handleChange }) {
  const [page, setPage] = useState(currentPage);

  return (
    <Stack spacing={2} sx={{ alignItems: 'center' }}>
      <Pagination
        count={count}
        page={page}
        onChange={handleChange}
        color="primary"
      />
    </Stack>
  );
}
