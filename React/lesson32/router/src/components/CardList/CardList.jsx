import './CardList.scss';
import { useNavigate } from 'react-router-dom';

function CardList({ cards }) {
  const navigate = useNavigate();

  function navigateToCardInfo(card) {
    navigate(`/dashboard/tv-show/${card.id}`);
  }

  return (
    <ul className="card-list">
      {cards.map((card) => (
        <li
          onClick={() => navigateToCardInfo(card)}
          className="card"
          key={card.id}
        >
          <a className="card-image">
            <img
              src={'https://image.tmdb.org/t/p/w500' + card.poster_path}
              alt={card.name}
            />
          </a>
          <a className="card-description">
            <h2>{card.name}</h2>
          </a>
        </li>
      ))}
    </ul>
  );
}

export default CardList;
