import { useRef, useEffect, useContext } from 'react';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { TodoContextComponent } from '../../context/Context';
import { updateTodo, addTodo, searchByName } from '../../reducer/todoReducer';
import './AddTodo.scss';

function AddTodo({ editedTodo, setEditedTodo }) {
  const [, dispatch] = useContext(TodoContextComponent);
  const [newTodo, setNewTodo] = useState(editedTodo ? editedTodo.text : '');
  const inputRef = useRef('');

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const onSubmit = function (event) {
    event.preventDefault();

    if (editedTodo) {
      const todo = {
        ...editedTodo,
        text: newTodo,
      };
      dispatch(updateTodo(todo));
      setEditedTodo(null);
    } else {
      const todo = {
        id: uuidv4(),
        text: newTodo,
        isCompleted: false,
      };
      //dispatch(addTodo(todo));
      dispatch(searchByName(todo.text));
    }
    setNewTodo('');
  };

  return (
    <form className="todo-form" onSubmit={onSubmit}>
      <input
        className="todo-input"
        placeholder="Add a todo"
        value={newTodo}
        onChange={(event) => setNewTodo(event.target.value)}
        ref={inputRef}
      />
      <button className="todo-button">
        {editedTodo ? 'Update' : 'Add Todo'}
      </button>
    </form>
  );
}

export default AddTodo;
