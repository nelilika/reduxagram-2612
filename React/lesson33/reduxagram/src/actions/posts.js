export const LOAD_POSTS = '[POSTS] Load Posts';

/**
 * JS Doc
 */
export const loadPosts = (posts) => ({
  type: LOAD_POSTS,
  payload: { posts },
});
