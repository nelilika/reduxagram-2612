import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getComments } from '../../api';

export const fetchComments = createAsyncThunk('comments/fetchComments', (id) =>
  getComments(id)
);

export const commentsSlice = createSlice({
  name: 'comments',
  initialState: {
    comments: [],
    error: null,
  },
  reducers: {
    loadComments: (state, action) => {
      state.comments = action.payload.comments;
    },
    failureComments: (state, action) => {
      state.comments = [];
      state.error = action.payload.error;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchComments.fulfilled, (state, action) => {
        state.comments = action.payload.comments;
      })
      .addCase(fetchComments.rejected, (state, action) => {
        state.comments = [];
        state.error = action.payload.error;
      });
  },
});

export const { loadComments, failureComments } = commentsSlice.actions;

export default commentsSlice.reducer;
