import React, { useEffect } from 'react';
import Button from '@mui/material/Button';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { Input } from '../UI/Input';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { addComment } from '../../actions/comments';

const schema = yup.object().shape({
  author: yup.string(),
  comment: yup.string().required('The comment is required'),
});

function AddNewComment({ defaultValues, selectedPost }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    mode: 'onBlur',
    resolver: yupResolver(schema),
    defaultValues,
  });
  const { user } = useSelector((state) => state.usersReducer);
  const dispatch = useDispatch();

  const onSubmit = ({ comment: text, author }) => {
    const comment = {
      postId: selectedPost.id,
      text,
      user: user.username ?? author,
    };
    dispatch(addComment(comment));
    reset();
  };

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <form noValidate onSubmit={handleSubmit(onSubmit)}>
      {!user.username && (
        <Input
          {...register('author')}
          id="author"
          type="text"
          label="Author"
          name="author"
          error={!!errors.author}
        />
      )}
      <Input
        {...register('comment')}
        id="comment"
        type="text"
        label="Your comment"
        name="comment"
        error={!!errors.comment}
      />
      <Button type="submit" fullWidth variant="contained" color="primary">
        Add new Post
      </Button>
    </form>
  );
}

export default AddNewComment;

AddNewComment.propTypes = {
  defaultValues: PropTypes.object,
  selectedPost: PropTypes.object,
};
