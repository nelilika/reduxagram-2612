// alert('Hello World'); /** показує модальне вікно */
// confirm('Are you sure?'); /** показує модальне вікно з кнопками так/ні */
// prompt('Enter you name', '23'); /** показує модальне вікно з інпутом*/
// console.log('Hello world');

// будь-який коментар

// Завдання 1
// Напишиіть програму ...


/* Багаторядковий
коментар
з 
програмування
*/


// Створювання змінних
var username = 'Werty'; // застарілий спосіб
let username1 = 'Werty'; // 
const username2 = 'Werty'; // константа, або змінна, яку ви не можете змінити
username; // взагалі не треба використовувати

// Правила
let username$_1234567 = ''; // змінна НЕ МОЖЕ починатися з цифри

// first_name; // snakeCase
// firstName; // camelCase - використовуємо тільки це
// first-name; // kebabCase

// Типи даних

console.log(typeof null); // object !!! але це тип даних null
// це офіційна помилка JavaScript

console.log(typeof function() {}); // function
// фактично function це тип даних об'єкт

// Динамічна типізація
let randomValue;
console.log(randomValue);
console.log(typeof randomValue); // 'undefined'
randomValue = 12;
console.log(randomValue);
console.log(typeof randomValue);

//randomValue = 2 + 2 * 2;

let number = 5;
number++; // - інкремент
number = number + 1;

// Практика
const number1 = +prompt('Enter first value');
console.log(number1);
const number2 = +prompt('Enter last value');
// prompt - завжди повертає рядок або string