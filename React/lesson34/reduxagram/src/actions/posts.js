export const LOAD_POSTS = '[POSTS] Load Posts';
export const FAILURE_POSTS = '[POSTS] Failure Posts';

/**
 * JS Doc
 */
export const loadPosts = (posts, totalCount) => ({
  type: LOAD_POSTS,
  payload: { posts, totalCount },
});

export const failurePosts = (error) => ({
  type: FAILURE_POSTS,
  payload: { error },
});
