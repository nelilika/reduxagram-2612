import { LOG_IN } from '../actions/users';

// початковий стейт
export const initialState = {
  user: {},
};

// редьюсер
export const usersReducer = function (state = initialState, action) {
  switch (action.type) {
    case LOG_IN:
      return {
        ...state,
        user: { ...action.payload.user },
      };
    default:
      return state;
  }
};
