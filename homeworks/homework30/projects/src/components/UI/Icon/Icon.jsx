import React from 'react';

function ProjectIcon({ children, width, height, className }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width ?? '20'}
      height={height ?? '20'}
      fill="none"
      stroke="currentColor"
      stroke-linecap="round"
      stroke-linejoin="round"
      stroke-width="2"
      className={className}
      viewBox="0 0 24 24"
    >
      {children}
    </svg>
  );
}

export default ProjectIcon;
