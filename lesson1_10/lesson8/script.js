// Функція
{
  const number = '123456';

  if (number.length && isFinite(number) && !(Number(number) % 1)) {
    const result =
      number >= 0 ? reverseNumber(number, 0) : `-${reverseNumber(number, 1)}`;
    console.log(result);
  } else {
    console.error('Not valid data');
  }
}

reverseNumber('123456', 5);
reverseNumber('-34564', 3);

function reverseNumber(numberAsStr, lastIndex) {
  let reversedNumber = '';
  for (let i = numberAsStr.length - 1; i >= lastIndex; i--) {
    reversedNumber += numberAsStr.charAt(i);
  }
  return reversedNumber;
}

let sum1 = calculateSum(); // 45
// let sum1 = calculateSumExpression();  -> буде помилка, бо спроба використовувати
// до її ініціалізації
console.log(++sum1); // 46

// як створити функцію
// function declaration - функція оголошення
function calculateSum() {
  let sum = 0;
  for (let i = 0; i < 10; i++) {
    sum += i;
  }
  return sum;
}

// function expression - функція вираз
const calculateSumExpression = function () {
  let sum = 0;
  for (let i = 0; i < 10; i++) {
    sum += i;
  }
  return sum;
};

// arguments
function log(a, b, c, d, e, f) {
  // let a = 3
  // let b = 5;
  // let c;
  // let d;
  console.log(a + b);
  console.log(arguments);
  for (let i = 0; i < arguments.length; i++) {
    console.log('number: ', arguments[i]);
  }

  const realArrayArgument = [...arguments];
  console.log(realArrayArgument.sort());
}
log(5, 4); // 9 - argumets - [4 ,5]
log(5, 3); // 8 - argumets - [3 ,5]
log(3, 5, 'q3', 24, 2346, 23, 2); // 8 - argumets - [3, 5, 'q3', 24, 2346, 2346, 23, 2]

// Стрілкова функція
// можна зробити тільки з function expression
// function expression
const sum = function (a, b) {
  return a + b;
};

const sumArrow = (a, b) => a + b;

const calculate = function (a, b) {
  return {
    sum: a + b,
    sub: a - b,
    mult: a * b,
    div: a / b,
  };
};

const calculateArrow = (a, b) => ({
  sum: a + b,
  sub: a - b,
  mult: a * b,
  div: a / b,
});

console.log(calculate);
console.dir(calculate); // виведе в консоль функцію як обʼєкт

// Інші види функції

// 1) функція-рекурсія
// 2) функція вищого порядку
// 3) функція планування
// 4) функція самовикликаюча

// 4) функція самовикликаюча
(function () {
  console.log('Hello from IIFE');
})();
