function Device(display) {
  this.display = display;
  this.isTurnedOn = false;
}

Device.prototype.turnOn = function () {
  console.log('turned on');
  this.isTurnedOn = true;
};

Device.prototype.turnOf = function () {
  console.log('turned off');
  this.isTurnedOn = false;
};

function Computer(mouse, keebord, system, display) {
  Device.call(this);
  this.mouse = mouse;
  this.keebord = keebord;
  this.system = system;
  this.display = display;
}

Computer.prototype = Device.prototype;
Computer.prototype.download = function (fileName) {
  console.log('downloaded file: ', fileName);
};

class DeviceClass {
  #isTurnedOn = false;

  constructor(display) {
    this.display = display;
  }

  turnOn() {
    console.log('turned on');
    this.#isTurnedOn = true;
  }

  turnOf() {
    console.log('turned off');
    this.#isTurnedOn = false;
  }
}

class ComputerClass extends DeviceClass {
  constructor(mouse, keebord, system, display) {
    super(display);
    this.mouse = mouse;
    this.keebord = keebord;
    this.system = system;
  }

  download(fileName) {
    console.log('downloaded file: ', fileName);
  }

  turnOn() {
    super.turnOn();
    console.log('turned on computer');
  }
}

const computerFake = Computer('Apple', 'Apple', 'Apple', 'Apple'); // вызов функции
const computer1 = new Computer('Apple', 'Apple', 'Apple', 'Apple'); // создание эксземпляра класса

const computer2 = new ComputerClass('Apple', 'Apple', 'Apple', 'Apple'); // создание эксземпляра класса

console.log(computer2);
