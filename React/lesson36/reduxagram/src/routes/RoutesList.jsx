import { useRoutes } from 'react-router-dom';
import { routes } from './Routes';

function RoutesList() {
  let element = useRoutes(routes);
  return element;
}

export default RoutesList;
