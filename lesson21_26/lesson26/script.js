// Map / Set
// Ітератори / Генератори
// Питання

// Що таке обʼєкт?
// ключами обʼєкту можуть бути тільки рядки
const obj = {
  0: 'qwerty0',
  1: 'qwerty1',
  2: 'qwerty2',
};

// Що таке массив?
// ключами обʼєкту можуть бути тільки рядки
const arr = [1, 2, 3];
// arr.property = '1234';

for (let key in arr) {
  console.log(typeof key);
}

// Map - коллекція даних, ключ-значення, де ключі можуть бути будь-яким (!) типом даних
// створюємо нову колекцію типу Map
const map = new Map();
// додаємо значення
console.log(typeof map);
map.set(8, 50);
map.set('fruit', 'melon');
map.set(true, undefined);

// цикл по ключах
for (let key of map.keys()) {
  console.log(key);
}

// цикл по значеннях
for (let value of map.values()) {
  console.log(value);
}

console.log(map.get(true));

map.delete('fruit');

// Set - колекція даних, яка зберігає тільки унікальні ключі

// створює нову колекцію
const set = new Set();
set.add(null);
set.add(null);
set.add('Lorem epsilum');
set.add(undefined);
set.add(undefined);
set.add(NaN);
set.add(NaN);

const notUniqueArray = [
  1,
  1,
  undefined,
  undefined,
  null,
  34,
  'qwerty',
  'Qwerty',
  NaN,
  NaN,
];

const uniqueArray1 = notUniqueArray.filter(
  (value, index, array) => array.indexOf(value) === index
); // ігнорує NaN
const uniqueSet = new Set(notUniqueArray);
console.log(typeof uniqueSet);

// const uniqueArray = [...new Set(notUniqueArray)];
const uniqueArray = Array.from(new Set(notUniqueArray));

// WeakMap - коллекція даних, ключ-значення, де ключі - тільки обʼєкти
// WeakSet - коллекція даних, які зберігають в якості значень тільки обʼєкти
// не ітеріруємі

// Ітератори / Генератори

// Symbol - притимітивний тип даних
const student = {
  name: 'Aloe',
  age: 25,
  [Symbol('id')]: '123456789',
  [Symbol('id')]: '123456789',
};

for (let key in student) {
  console.log('Student key:', key);
}

const symbolIds = Object.getOwnPropertySymbols(student); // поверне масив всіх символів обʼєкту
// symbolIds[0] === symbolIds[1] - false

// iтерируємі обʼєкти - Array, Map, Set
// не ітеріруємі обʼєкти - Object

const arrIterator = arr[Symbol.iterator]();

for (let value of arr) {
  console.log('For of:', value);
}

// приклад того як for of працює під капотом
let result = arrIterator.next(); // { value: <значення масиву>, done: true/false }
while (!result.done) {
  console.log('Iterator next: ', result.value);

  result = arrIterator.next();
}

const iterationObj = {
  min: 1,
  max: 11,
  [Symbol.iterator]() {
    return {
      current: this.min,
      last: this.max,
      next() {
        if (this.current <= this.last) {
          return { value: this.current++, done: false };
        }
        return { value: undefined, done: true };
      },
    };
  },
};

console.log(iterationObj);

for (let value of iterationObj) {
  console.log(value);
}

// Генератори - функція, яка тає точку остановки у вигляді yield
function* getNumber() {
  console.log('Start');

  let a = 'value of a';
  let b = 15;

  yield a;
  console.log('calculate c');

  let c = a + b;

  yield c; // return c - закінчить ітерацію

  yield b;
}

const generateNumber = getNumber();
console.log(generateNumber);

for (let value of generateNumber) {
  console.log('Value', value);
}
