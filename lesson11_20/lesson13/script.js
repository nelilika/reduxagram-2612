// Array.prototype.sort();
// Array.prototype.reduce();
// EventLoop
// загублення контексту (this)

const array = ['a', 'c', '7', '%', '34', 'Aa', 'qQ'];

const sortedArray = [...array].sort();

console.log(array);
console.log(sortedArray);

// Сортування чисел
const numberArray = [4, 2, 3, 956, 45, 97, 134];

const incorrectedSortedNumberArray = [...numberArray].sort();
// за замовчування дані сортуються як рядки за правилом порівняння рядків
// від меншого до більшого

console.log(numberArray);
console.log(incorrectedSortedNumberArray);

const correctedSortedNumberArray = [...numberArray].sort(sortNumber);
// можна взяти будь-яке число, але важливо щоб воно було число і додатньє/відʼємне

console.log(numberArray);
console.log(correctedSortedNumberArray);

function sortNumber(currectItem, nextItem) {
  // return currectItem - nextItem;
  if (currectItem > nextItem) {
    return -4;
  }
  if (currectItem < nextItem) {
    return 4;
  }
  return 0;
}

// Reduce()
console.log('numberArray for reduce: ', numberArray);

// acc за дефолтом - це перший елемент масиву
const sum = numberArray.reduce(function (acc, item, index) {
  console.log(acc);
  return acc + item; // обовʼязково повертаємо acc
});

// const concatenation = numberArray.join('');
const concatenation = numberArray.reduce(function (acc, item) {
  console.log(acc);
  return acc + item; // обовʼязково повертаємо acc
}, '');

// const filterMap = numberArray
//   .filter((item) => item > 100)
//   .map((item) => String(item));

const filterMap = numberArray.reduce(function (acc, item) {
  console.log(acc); // [];

  if (item > 100) {
    return [...acc, String(item)]; // push()
  }
  return acc;
}, []);

console.log(filterMap);

/*
{
  'Karan Duffy': 1010 * 10,
}
*/
const employeesInfo = employees.reduce(function (acc, employee) {
  console.log(acc); // {};

  // acc[`${employee.name} ${employee.surname}`] =
  //   employee.workExperience * employee.salary;

  // return acc;
  return {
    ...acc,
    [`${employee.name} ${employee.surname}`]:
      employee.workExperience * employee.salary,
  };
}, {});

const employeesInfoStr = employees.reduce(function (acc, employee) {
  return (
    acc +
    `${employee.name} ${employee.surname} - ${
      employee.workExperience * employee.salary
    }\n`
  );
}, '');

console.log(employeesInfo);

// EventLoop - цикл подій
// асінхронний код -> не сінхронний код

// приклад асінхронності
// 1) setTimeout / setInterval()
// 2) events browser
// 3) promises

// EventLoop

function main() {
  function showText() {
    setTimeout(function log() {
      console.log('hello in 4sec');
    }, 4000); // -> Web API

    setTimeout(function log() {
      console.log('hello');
    }, 1000); // -> Web API
    ///
    ///
    ///
    setTimeout(function log() {
      console.log('hello in 2sec.1');
    }, 2000); // -> Web API

    setTimeout(function log() {
      console.log('hello in 2sec.2');
    }, 2000); // -> Web API
  }
  showText();
}
main();
// end

//
console.log(1);

log();
function log() {
  console.log(2);
}

setTimeout(() => {
  console.log(3);
}, 0);

console.log(4);
