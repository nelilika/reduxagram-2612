import React, { useEffect, useState, useMemo } from 'react';
import PostGrid from '../components/Posts/PostGrid';
import Typography from '@mui/material/Typography';
import { useDispatch } from 'react-redux';
import { fetchPosts } from '../thunk/posts';
import { getPages } from '../utils/getTotalCount';
import { useSelector } from 'react-redux';
import PostPagination from '../components/UI/Pagination';

export default function Posts() {
  const { limit, totalCount } = useSelector((state) => state.postsReducer);
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);

  const pages = useMemo(() => getPages(limit, totalCount), [limit, totalCount]);

  useEffect(() => {
    dispatch(fetchPosts({ limit, page })); // це початковий для асінхронного запиту екшн
    // який викликає реквест на сервер
  }, [dispatch, limit, page]);

  return (
    <>
      <Typography
        variant="h1"
        component="h2"
        sx={{
          textAlign: 'center',
          fontFamily: 'Festive',
          color: 'lightBlue',
          m: '15px 0',
        }}
      >
        Reduxagram
      </Typography>
      <PostPagination
        count={pages}
        page={page}
        handleChange={(_, value) => setPage(value)}
      />
      <PostGrid />
    </>
  );
}
