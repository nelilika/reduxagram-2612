import React from 'react';
import Grid from '@mui/material/Grid';
import Post from './Post';
import { useSelector } from 'react-redux';

function PostGrid() {
  const { posts, error } = useSelector((state) => state.postsReducer);

  console.log(error);

  if (error) {
    return <h1>Something went wrong</h1>;
  }

  return (
    <>
      <Grid container spacing={2}>
        {posts.map((post) => (
          <Grid key={post.id} item xs={6} sm={4} md={3}>
            <Post post={post} />
          </Grid>
        ))}
      </Grid>
    </>
  );
}

export default PostGrid;
