export const LOG_IN = '[USERS] LOG IN';

/**
 * JS Doc
 */
export const loginUser = (user) => ({
  type: LOG_IN,
  payload: { user },
});
