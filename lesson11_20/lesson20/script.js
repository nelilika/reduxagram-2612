const cities = ['Kiyv', 'London', 'Helsinki'];
// cities[[Prototype]] - приватна властивіть, яка показує посилання на конструктор батьківського класу
// недоступна для читання і редагування

// Array.prototype === cities[[Prototype]]; // true
// Array.prototype === cities.__proto__; // true
// cities.__proto__.__proto__ === Object.prototype // true

// Приклади нативних функція-конструкторів в JavaScript
// Number, String, Boolean, Array, Object, RegExp, Function, Date, WebSocket

// Array.prototype.constructor - саме тут створюється контекст для майбутніх екземплярів обʼєкту

// стрілочка функція не має свого контексту і властивості prototype

// властивість prototype є у КОЖНОЇ функції створеної за допомогою ключового слова function

const getName = () => {
  console.log('Some name');
};
// new getName() - буде помилка

const getSurname = function () {
  console.log('Some surname');
}; // у анонімної функції є prototype

// не рекомендується (!) розширяти нативні функції-конструктори
// Array.prototype.filter = 5;

// instanceof - перевіряє в цепочке прототивів чи наслідується надий обʼєкт від заданого класу
// або Object.isInstanceOf()
cities instanceof Object; // true
cities instanceof Number; // false
cities instanceof String; // false
cities instanceof Array; // true

// ___
let number = 5;
number instanceof Number; // false - шукає тільки серед обʼєктів
Number.prototype === number.__proto__; // true

typeof Object.prototype === typeof {};

// Object.prototype.__proto__ - null

// Object.create(Array.prototype)

const objectWithoutPrototype = Object.create(null);
// objectWithoutPrototype.__proto__ // undefined
