class User {
  static userId = 1;
  constructor(firstName, lastName, passport) {
    this.id = User.userId++;
    this.firstName = firstName;
    this.lastName = lastName;
    this.passport = passport;
  }
}

/**
 * #money +
 * balance() +
 * withdraw() +
 * credit() +
 * history
 */

class BankAccount {
  #money = 0;
  #cardId = 1;
  static cardId = 1;
  #history = [];

  constructor(type, currency, user) {
    this.type = type;
    this.currency = currency;
    this.user = user;
    this.cardId = BankAccount.cardId++; // BankAccount.cardId
  }

  balance() {
    // BankAccount.prototype.balance
    return this.#money;
  }

  getHistory() {
    return this.#history;
  }

  withdraw(amount, persent) {
    if (isNaN(amount)) {
      return new Error('You must enter only the number');
    }

    if (amount > this.#money) {
      return new Error(
        `You can not withdraw ${amount} ${this.currency}. Max value is: ${
          this.#money
        }`
      );
    }
    this.#money = this.#money - amount - persent;
    this.#addToHistory(this.withdraw.name, amount);
  }

  credit(amount) {
    if (isNaN(amount)) {
      return new Error('You must enter only the number');
    }

    this.#money += amount;
    this.#addToHistory(this.credit.name, amount);
  }

  #addToHistory(name, amount) {
    this.#history.push({
      date: new Date().getTime(),
      message: `Dear, ${this.user.firstName} ${this.user.lastName} - ${name}: ${amount}`,
    });
  }

  calculatePercent(amount, percent) {
    return (amount / 100) * percent;
  }

  static logInfo() {
    console.log('Hello Im static method');
  }
}

class InternationalBankAccount extends BankAccount {
  #tax = '4.6%';

  constructor(type, currency, user) {
    super(type, currency, user);
  }

  withdraw(amount) {
    const tax = super.calculatePercent(amount, parseFloat(this.#tax));
    super.withdraw(amount, tax);
  }
}

console.log(BankAccount.cardId);

const user1 = new User('Anna', 'Kowalska', 'KT123456');
const user2 = new User('Piot', 'Kowal', 'QW654321');
const card1 = new BankAccount('VISA', 'UAN', user1);
const card2 = new BankAccount('MasterCard', 'USD', user2);

const internationalBankAccount = new InternationalBankAccount(
  'Maestro',
  'EURO',
  user1
);
console.log(internationalBankAccount);

console.log(card1);
console.log(card2);
