import React from 'react';

function NotFound() {
  return (
    <h1 style={{ color: 'red', margin: '20px auto', width: '60%' }}>
      404: Not found any page
    </h1>
  );
}

export default NotFound;
