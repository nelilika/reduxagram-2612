console.log('Lesson 12');

const array = [1, 2, 3, 4, 6, 10, 123];

// Accept Both
function filterArrayWithBigConflicts(array = []) {
  return array.filter((item) => item > 600);
}

filterArrayWithBigConflicts([1, 2, 3, 4]);

function counter() {
  let x = 10;
  console.log('x', x);
  let sum = 0;

  function calculateSum(number) {
    // debugger; - breakpoint
    sum += number;
    console.log(sum);
    return sum;
  }

  return calculateSum;
}

const calculateSum = counter(); // sum + number

calculateSum(3); // 3
calculateSum(4); // 7
calculateSum(7); // 14

// const value = prompt('Enter value', 6);

// if (value === 6) {
//   // '6' === 6
//   console.log('Hello');
// }

// Деструктуризация
const student = {
  name: 'Vovk',
  age: 25,
};

// const name = student.name; // Vovk
const { name } = student;
const { name: firstName } = student; // firstName = 'Vovk'

const [firstValue, secondValue] = [1, 2, 3, 4]; // firstValue = 1, secondValue = 2

// Call Stack
// (LIFO)

// last input
// first output

// queue  - LILO
// тарілки які стоять один на одному - LIFO - приклад стеку

function fizz() {
  console.log('fizz');

  function buzz() {
    console.log('buz');

    function fizzBuzz() {
      console.log('fizzBuzz');
    }

    fizzBuzz();
    console.log('buzz end');
  }

  buzz();
}

fizz();
